import Jira from 'src/common/net/Jira'
import Bitbucket from 'src/common/net/Bitbucket'
import ShowSettings from 'src/controllers/addon/ShowSettings'
import Disconnect from 'src/controllers/addon/Disconnect'

jest.mock('src/common/analytics')
jest.mock('src/common/net/Jira')
jest.mock('src/common/net/Bitbucket')
jest.mock('src/controllers/addon/ShowSettings')

const showSettingsCard = Symbol('show settings card')
const reset = jest.fn()
const execute = jest.fn().mockReturnValue(showSettingsCard)

Bitbucket.oauthService = jest.fn().mockReturnValue({ reset })
Jira.oauthService = jest.fn().mockReturnValue({ reset })
ShowSettings.mockImplementation(() => ({ execute }))

describe('Controller addon.Disconnect', () => {
  beforeEach(() => {
    Jira.mockClear()
    Bitbucket.mockClear()
    ShowSettings.mockClear()
  })

  it('Should reset Jira OAuth service', () => {
    new Disconnect().execute({ product: 'Jira' })

    expect(reset).toBeCalled()
  })

  it('Should reset Bitbucket OAuth service', () => {
    new Disconnect().execute({ product: 'Bitbucket' })

    expect(reset).toBeCalled()
  })

  it('Should throw an exception trying to reset the unknown product', () => {
    try {
      new Disconnect().execute({ product: 'Dummy' })
    } catch (error) {
      expect(error.message).toBe('Unknown product Dummy')
    }
  })

  it('Should return ShowSettings card on Jira OAuth service reset', () => {
    const card = new Disconnect().execute({ product: 'Jira' })

    expect(card).toBe(showSettingsCard)
  })

  it('Should return ShowSettings card on Bitbucket OAuth service reset', () => {
    const card = new Disconnect().execute({ product: 'Bitbucket' })

    expect(card).toBe(showSettingsCard)
  })
})
