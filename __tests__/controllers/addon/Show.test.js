import 'core-js/modules/es7.array.flat-map'
import Gmail from 'src/common/net/Gmail'
import Jira from 'src/common/net/Jira'
import Bitbucket from 'src/common/net/Bitbucket'
import { userProperties } from 'src/common/properties'

import AddonShowController from 'src/controllers/addon/Show'
import PrShowController from 'src/controllers/bitbucket/pullRequest/Show'
import PipelineShowController from 'src/controllers/bitbucket/pipeline/Show'
import BitbucketIssueShowController from 'src/controllers/bitbucket/issue/Show'
import IssueShowController from 'src/controllers/jira/issue/Show'
import PrPreviewController from 'src/controllers/bitbucket/pullRequest/Preview'
import IssuePreviewController from 'src/controllers/jira/issue/Preview'

import jiraProjectsService from 'src/services/jiraProjects'

import PreviewsView from 'src/views/addon/Previews'
import BlankView from 'src/views/addon/Blank'
import BitbucketOnboardingView from 'src/views/addon/onboarding/Bitbucket'
import JiraOnboardingView from 'src/views/addon/onboarding/Jira'

jest.mock('src/common/analytics')
jest.mock('src/common/properties')
jest.mock('src/common/net/Gmail')
jest.mock('src/common/net/Bitbucket')

jest.mock('src/controllers/bitbucket/pullRequest/Show')
jest.mock('src/controllers/bitbucket/pipeline/Show')
jest.mock('src/controllers/bitbucket/issue/Show')
jest.mock('src/controllers/jira/issue/Show')
jest.mock('src/controllers/bitbucket/pullRequest/Preview')
jest.mock('src/controllers/jira/issue/Preview')

jest.mock('src/services/jiraProjects')

jest.mock('src/views/addon/Previews')
jest.mock('src/views/addon/Blank')
jest.mock('src/views/addon/onboarding/Bitbucket')
jest.mock('src/views/addon/onboarding/Jira')

const getMessage = jest.fn()
Gmail.mockImplementation(() => ({ getMessage }))

Bitbucket.oauthService = jest.fn().mockReturnValue({ getAuthorizationUrl: () => 'dummy' })
Jira.oauthService = jest.fn().mockReturnValue({ getAuthorizationUrl: () => 'dummy' })

jiraProjectsService.findInstancesByProject.mockReturnValue([])

const event = {
  messageMetadata: {
    messageId: 'messageId',
    accessToken: 'accessToken',
  },
}

describe('Controller addon.Show', () => {
  beforeEach(() => jest.clearAllMocks())

  it('Should return Blank view and sync Jira projects when no links in email', () => {
    userProperties.get.mockReturnValue('dummy')
    getMessage.mockReturnValue({
      getBody: () => 'hello bitbucket and jira',
    })
    const result = new AddonShowController({ event }).execute()

    expect(result).toBeInstanceOf(BlankView)
    expect(BlankView).toBeCalled()
    expect(jiraProjectsService.sync).toBeCalled()
  })

  it('Should return PR View when PR link present', () => {
    getMessage.mockReturnValue({
      getBody: () => 'lorem ipsum https://bitbucket.org/owner/repo/pull-requests/320/pr-title',
    })
    const prView = Symbol('pr view')
    const prShowExecute = jest.fn().mockReturnValue(prView)
    PrShowController.mockImplementation(() => ({ execute: prShowExecute }))

    const result = new AddonShowController({ event }).execute()

    expect(PrShowController).toBeCalled()
    expect(prShowExecute).toBeCalledWith({
      owner: 'owner',
      repo: 'repo',
      id: '320',
    })
    expect(result).toEqual(prView)
  })

  it('Should return Pipeline View when Pipeline link present', () => {
    getMessage.mockReturnValue({
      getBody: () => 'hello https://bitbucket.org/owner/repo/addon/pipelines/home#!/results/{pipeline-id}/from-email world',
    })
    const pipelineView = Symbol('pipeline view')
    const pipelineShowExecute = jest.fn().mockReturnValue(pipelineView)
    PipelineShowController.mockImplementation(() => ({ execute: pipelineShowExecute }))

    const result = new AddonShowController({ event }).execute()

    expect(PipelineShowController).toBeCalled()
    expect(pipelineShowExecute).toBeCalledWith({
      owner: 'owner',
      repo: 'repo',
      id: '{pipeline-id}',
    })
    expect(result).toEqual(pipelineView)
  })

  it('Should return Bitbucket issue View when corresponding link is present', () => {
    getMessage.mockReturnValue({
      getBody: () => 'https://bitbucket.org/owner/repo/issues/123',
    })
    const bitbucketIssueView = Symbol('bitbucket issue view')
    const bitbucketIssueExecute = jest.fn().mockReturnValue(bitbucketIssueView)
    BitbucketIssueShowController.mockImplementation(() => ({ execute: bitbucketIssueExecute }))

    const result = new AddonShowController({ event }).execute()

    expect(BitbucketIssueShowController).toBeCalled()
    expect(bitbucketIssueExecute).toBeCalledWith({
      owner: 'owner',
      repo: 'repo',
      id: '123',
    })
    expect(result).toEqual(bitbucketIssueView)
  })

  it('Should return Jira Issue View when Issue link is present', () => {
    getMessage.mockReturnValue({
      getBody: () => 'https://dummy.atlassian.net/browse/DEV-14',
    })
    const issueView = Symbol('issue view')
    const issueShowExecute = jest.fn().mockReturnValue(issueView)
    IssueShowController.mockImplementation(() => ({ execute: issueShowExecute }))

    const result = new AddonShowController({ event }).execute()

    expect(IssueShowController).toBeCalled()
    expect(issueShowExecute).toBeCalledWith({
      instanceHost: 'dummy.atlassian.net',
      issueKey: 'DEV-14',
    })
    expect(result).toEqual(issueView)
  })

  it('Should return Previews view when several links are present', () => {
    getMessage.mockReturnValue({
      getBody: () => 'https://dummy2.atlassian.net/browse/DEV-1 https://bitbucket.org/owner2/repo2/pull-requests/32',
    })
    const issuePreviewExecute = jest.fn().mockReturnValue({ issue: 'issue' })
    const prPreviewExecute = jest.fn().mockReturnValue({ pr: 'pr' })
    IssuePreviewController.mockImplementation(() => ({ execute: issuePreviewExecute }))
    PrPreviewController.mockImplementation(() => ({ execute: prPreviewExecute }))

    const result = new AddonShowController({ event }).execute()

    expect(result).toBeInstanceOf(PreviewsView)

    expect(IssuePreviewController).toBeCalled()
    expect(issuePreviewExecute).toBeCalledWith({ instanceHost: 'dummy2.atlassian.net', issueKey: 'DEV-1' })
    expect(PrPreviewController).toBeCalled()
    expect(prPreviewExecute).toBeCalledWith({ id: '32', owner: 'owner2', repo: 'repo2' })
  })

  it('Should call jiraProjects service with issue keys when Jira issue keys are present', () => {
    getMessage.mockReturnValue({
      getBody: () => 'hello world DEV-1 DEV-2',
    })

    new AddonShowController({ event }).execute()
    expect(jiraProjectsService.findInstancesByProject).toBeCalledWith('DEV')
  })

  it('Should return Jira Issue view when issue key is present', () => {
    getMessage.mockReturnValue({
      getBody: () => 'hello world DEV-1',
    })
    jiraProjectsService.findInstancesByProject.mockReturnValue(['test-instance.atlassian.net'])
    const issueView = Symbol('issue-view')
    const issueShowExecute = jest.fn().mockReturnValue(issueView)
    IssueShowController.mockImplementation(() => ({ execute: issueShowExecute }))

    const result = new AddonShowController({ event }).execute()

    expect(result).toBe(issueView)
    expect(IssueShowController).toBeCalled()
    expect(issueShowExecute).toBeCalledWith({
      instanceHost: 'test-instance.atlassian.net',
      issueKey: 'DEV-1',
    })
  })

  it(`Should get 'onboardingState' property from userProperties`, () => {
    new AddonShowController({ event }).execute()

    expect(userProperties.get).toBeCalledWith('onboardingState')
  })

  it(`Should return Jira onboarding view when 'onboardingState' doesnt't exist`, () => {
    userProperties.get.mockReturnValue(null)
    expect(new AddonShowController({ event }).execute()).toBeInstanceOf(JiraOnboardingView)
  })

  it(`Should return Bitbucket onboarding view when 'onboardingState' set to 'bitbucket'`, () => {
    userProperties.get.mockReturnValue('bitbucket')
    expect(new AddonShowController({ event }).execute()).toBeInstanceOf(BitbucketOnboardingView)
  })
})
