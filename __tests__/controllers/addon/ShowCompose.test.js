import 'core-js/modules/es7.array.flat-map'
import ShowCompose from 'src/controllers/addon/ShowCompose'
import LinkComposeView from 'src/views/addon/LinkCompose'
import Jira from 'src/common/net/Jira'

jest.mock('src/common/net/Jira')
jest.mock('src/views/addon/LinkCompose')

const accessibleResources = [{ id: '1337' }]
const getAccessibleResources = jest.fn().mockReturnValue(accessibleResources)
const parallel = jest.fn().mockReturnThis()
const searchIssues = jest.fn().mockReturnThis()
const fetch = jest.fn().mockReturnValue([])
Jira.mockImplementation(() => ({ getAccessibleResources, searchIssues, parallel, fetch }))

describe('Controller addon.ShowCompose', () => {
  beforeEach(() => jest.clearAllMocks())

  it('Should return LinkCompose view', () => {
    expect(new ShowCompose().execute({})).toBeInstanceOf(LinkComposeView)
  })

  it('Should call LinkCompose view with correct params', () => {
    fetch.mockReturnValueOnce([
      { issues: [{ key: 'TEST-1', fields: {} }] },
      { issues: [{ key: 'TEST-2', fields: {} }] },
    ])

    new ShowCompose().execute({})
    expect(LinkComposeView).toBeCalledWith({
      issues: [
        { fields: {}, key: 'TEST-1' },
        { fields: {}, key: 'TEST-2' },
      ],
    })
  })

  it('Should call Jira with correct params', () => {
    getAccessibleResources.mockReturnValueOnce([
      { id: 'resource-1' },
      { id: 'resource-2' },
    ])

    const queryParams = {
      fields: 'summary,issuetype,project,lastViewed',
      jql: 'summary ~ "dev*" OR project = "dev"  ORDER BY lastViewed',
      maxResults: 10,
      validateQuery: 'none',
    }

    new ShowCompose().execute({ issueQuery: 'dev' })
    expect(getAccessibleResources).toBeCalled()
    expect(searchIssues).toHaveBeenCalledTimes(2)
    expect(searchIssues.mock.calls[0]).toEqual([queryParams, { cloudId: 'resource-1' }])
    expect(searchIssues.mock.calls[1]).toEqual([queryParams, { cloudId: 'resource-2' }])
  })
})
