import Bitbucket from 'src/common/net/Bitbucket'
import Jira from 'src/common/net/Jira'
import ShowSettingsView from 'src/views/addon/Settings'
import ShowSettings from 'src/controllers/addon/ShowSettings'

jest.mock('src/common/analytics')
jest.mock('src/common/net/Bitbucket')
jest.mock('src/common/net/Jira')
jest.mock('src/views/addon/Settings')

// Bitbucket part
const bitbucketUser = { display_name: 'Test Name' }
const emails = { values: [{ email: 'test@google.com', is_primary: true }] }
const bitbucketAuthUrl = Symbol('authorization bitbucket url')
const getCurrentBitbucketUser = jest.fn().mockReturnThis()
const getCurrentBitbucketUserEmails = jest.fn().mockReturnThis()
const bitbucketParallel = jest.fn().mockReturnThis()
const bitbucketFetch = jest.fn().mockReturnValue([bitbucketUser, emails])
Bitbucket.mockImplementation(() => ({
  getCurrentUser: getCurrentBitbucketUser,
  getCurrentUserEmails: getCurrentBitbucketUserEmails,
  parallel: bitbucketParallel,
  fetch: bitbucketFetch,
}))
Bitbucket.oauthService = jest.fn().mockReturnValue({
  getAuthorizationUrl: jest.fn().mockReturnValue(bitbucketAuthUrl),
})

// Jira part
const jiraUser = { name: 'Test User' }
const resources = [{ id: 'c51c0262-20cd-462d-b989-5a0aadcffec9' }]
const jiraAuthUrl = Symbol('authorization jira url')
const getServerInfo = jest.fn()
const getCurrentJiraUser = jest.fn().mockReturnThis()
const getAccessibleResources = jest.fn().mockReturnThis()
const jiraFetch = jest.fn().mockReturnValue([jiraUser, resources])
const jiraParallel = jest.fn().mockReturnThis()
Jira.mockImplementation(() => ({
  getCurrentUser: getCurrentJiraUser,
  parallel: jiraParallel,
  getAccessibleResources,
  fetch: jiraFetch,
  getServerInfo,
}))
Jira.oauthService = jest.fn().mockReturnValue({
  getAuthorizationUrl: jest.fn().mockReturnValue(jiraAuthUrl),
})

describe('Controller addon.ShowSettings', () => {
  beforeEach(() => jest.clearAllMocks())

  it('Returns show settings card view', () => {
    expect(new ShowSettings().execute()).toBeInstanceOf(ShowSettingsView)
  })

  it('It checks that the SettingsView constructor has been called with a proper parameters', () => {
    new ShowSettings().execute()

    expect(ShowSettingsView).toBeCalledWith({
      bitbucketUser,
      bitbucketAuthUrl,
      jiraUser,
      jiraAuthUrl,
    })
  })

  it('It checks the jira methods calls', () => {
    new ShowSettings().execute()

    expect(getServerInfo).toHaveBeenCalledWith({ cloudId: resources[0].id })
    expect(getServerInfo).toHaveBeenCalled()
    expect(jiraFetch).toHaveBeenCalledTimes(2)
    expect(getAccessibleResources).toHaveBeenCalled()
    expect(getCurrentJiraUser).toHaveBeenCalled()
    expect(jiraParallel).toHaveBeenCalledTimes(2)
  })

  it('It checks the bitbucket methods calls', () => {
    new ShowSettings().execute()

    expect(getCurrentBitbucketUser).toHaveBeenCalled()
    expect(getCurrentBitbucketUserEmails).toHaveBeenCalled()
    expect(bitbucketParallel).toHaveBeenCalled()
    expect(bitbucketFetch).toHaveBeenCalled()
  })
})
