import FeedbackShowController from 'src/controllers/addon/feedback/Show'
import FeedbackView from 'src/views/addon/feedback/Feedback'
import Jira from 'src/common/net/Jira'
import Bitbucket from 'src/common/net/Bitbucket'

jest.mock('src/common/analytics')
jest.mock('src/common/net/Jira')
jest.mock('src/common/net/Bitbucket')
jest.mock('src/views/addon/feedback/Feedback')

const currentUser = {
  account_id: Symbol('account_id'),
}

const unauthErr = Object.assign(new Error(), { code: 'UNAUTHORIZED' })
const notFoundErr = Object.assign(new Error(), { code: 'NOT_FOUND' })

const getCurrentJiraUser = jest.fn()
const getCurrentBitbucketUser = jest.fn()

Jira.mockImplementation(() => ({ getCurrentUser: getCurrentJiraUser }))
Bitbucket.mockImplementation(() => ({ getCurrentUser: getCurrentBitbucketUser }))

describe('Controller addon.feedback.Show', () => {
  beforeEach(() => {
    Jira.mockClear()
    Bitbucket.mockClear()
    FeedbackView.mockClear()
  })

  it('Should return feedback view when user logged into Jira', () => {
    getCurrentJiraUser.mockImplementationOnce(() => currentUser)

    expect(new FeedbackShowController().execute()).toBeInstanceOf(FeedbackView)
  })

  it('Should should call Jira to get current user account_id and return feedback view', () => {
    getCurrentJiraUser.mockImplementationOnce(() => currentUser)

    new FeedbackShowController().execute()

    expect(Jira).toBeCalled()
    expect(getCurrentJiraUser).toBeCalled()
    expect(FeedbackView).toBeCalledWith({
      currentUserId: currentUser.account_id,
    })
  })

  it('Should should call Bitbucket to get current user account_id and return feedback view', () => {
    getCurrentJiraUser.mockImplementationOnce(() => { throw unauthErr })
    getCurrentBitbucketUser.mockImplementationOnce(() => currentUser)

    new FeedbackShowController().execute()

    expect(Jira).toBeCalled()
    expect(Bitbucket).toBeCalled()
    expect(getCurrentJiraUser).toBeCalled()
    expect(getCurrentBitbucketUser).toBeCalled()
    expect(FeedbackView).toBeCalledWith({
      currentUserId: currentUser.account_id,
    })
  })

  it(`Should return feedback view if user wasn't logged in neither in Jir nor in Bitbucket`, () => {
    getCurrentJiraUser.mockImplementationOnce(() => { throw unauthErr })
    getCurrentBitbucketUser.mockImplementationOnce(() => { throw unauthErr })

    new FeedbackShowController().execute()

    expect(Jira).toBeCalled()
    expect(Bitbucket).toBeCalled()
    expect(getCurrentJiraUser).toBeCalled()
    expect(getCurrentBitbucketUser).toBeCalled()
    expect(FeedbackView).toBeCalledWith({ currentUserId: undefined })
  })

  it('Should throw an error when Jira user is not found', () => {
    getCurrentJiraUser.mockImplementationOnce(() => { throw notFoundErr })

    const execute = () => new FeedbackShowController().execute()

    expect(execute).toThrowError()
  })

  it('Should throw an error when Bitbucket user is not found', () => {
    getCurrentBitbucketUser.mockImplementationOnce(() => { throw notFoundErr })

    const execute = () => new FeedbackShowController().execute()

    expect(execute).toThrowError()
  })
})
