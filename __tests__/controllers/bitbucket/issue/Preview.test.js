import Bitbucket from 'src/common/net/Bitbucket'
import PreviewController from 'src/controllers/bitbucket/issue/Preview'
import issue from '../../../helpers/mocks/responses/bitbucket/getIssue.json'

jest.mock('src/common/net/Bitbucket')

const params = {
  owner: 'owner',
  repo: 'repo',
  id: 'id',
}

const getIssue = jest.fn().mockReturnValue(issue)
Bitbucket.mockImplementation(() => ({ getIssue }))

describe('Controller bitbucket.issue.Show', () => {
  beforeEach(() => Bitbucket.mockClear())

  it('Should call bitbucket', () => {
    new PreviewController().execute(params)
    expect(Bitbucket).toBeCalledWith(params.owner, params.repo)
    expect(getIssue).toBeCalledWith(params.id, { fields: 'repository,title,id,state,updated_on' })
  })

  it('Should return preview object', () => {
    expect(new PreviewController().execute(params)).toEqual({
      key: 'Issue #3',
      source: 'test-repo',
      status: 'new',
      title: 'Third issue',
      updatedAt: '2018-06-26T08:55:31.959348+00:00',
    })
  })
})
