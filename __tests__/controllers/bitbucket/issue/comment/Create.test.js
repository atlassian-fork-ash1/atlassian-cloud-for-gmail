import Bitbucket from 'src/common/net/Bitbucket'
import CommentCreateController from 'src/controllers/bitbucket/issue/comment/Create'
import IssueShowController from 'src/controllers/bitbucket/issue/Show'
import AddonError from 'src/common/AddonError'

jest.mock('src/common/analytics')
jest.mock('src/common/net/Bitbucket')
jest.mock('src/controllers/bitbucket/issue/Show')

const params = {
  owner: 'owner',
  repo: 'repo',
  id: 'id',
}

const extendedParams = {
  ...params,
  content: 'lorem ipsum',
}

const createIssueComment = jest.fn()
Bitbucket.mockImplementation(() => ({ createIssueComment }))

describe('Controller bitbucket.issue.comment.Create', () => {
  beforeEach(() => {
    Bitbucket.mockClear()
  })

  it('Should call Bitbucket with correct params', () => {
    new CommentCreateController().execute(extendedParams)

    expect(Bitbucket).toBeCalledWith(params.owner, params.repo)
    expect(createIssueComment).toBeCalledWith(params.id, extendedParams.content)
  })

  it('Should call issue.Show controller', () => {
    const execute = jest.fn()
    IssueShowController.mockImplementation(() => ({ execute }))

    new CommentCreateController().execute(extendedParams)

    expect(IssueShowController).toBeCalled()
    expect(execute).toBeCalledWith(params)
  })

  it('Should throw AddonError when no content provided', () => {
    const execute = () => new CommentCreateController().execute({ ...extendedParams, content: '' })

    expect(execute).toThrow(AddonError)
    expect(execute).toThrowErrorMatchingSnapshot()
  })
})
