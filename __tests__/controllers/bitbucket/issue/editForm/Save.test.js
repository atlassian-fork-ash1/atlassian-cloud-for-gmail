import EditFormSaveController from 'src/controllers/bitbucket/issue/editForm/Save'
import IssueShowController from 'src/controllers/bitbucket/issue/Show'
import Bitbucket from 'src/common/net/Bitbucket'

jest.mock('src/common/analytics')
jest.mock('src/common/net/Bitbucket')
jest.mock('src/views/bitbucket/issue/Issue')
jest.mock('src/controllers/bitbucket/issue/Show')

const params = {
  owner: 'owner',
  repo: 'repo',
  id: 'id',
  kind: 'test-kind',
  priority: 'test-priority',
  title: 'test-title',
  content: 'test-description',
}

IssueShowController.mockImplementation(() => ({ execute: jest.fn() }))

const updateIssue = jest.fn()
Bitbucket.mockImplementation(() => ({ updateIssue }))

describe('Controller bitbucket.Issue.editForm.Save', () => {
  beforeEach(() => jest.clearAllMocks())

  it('Should call Bitbucket with correct params', () => {
    new EditFormSaveController().execute(params)

    expect(Bitbucket).toBeCalledWith(params.owner, params.repo)
    expect(updateIssue).toBeCalledWith('id', {
      kind: 'test-kind',
      priority: 'test-priority',
      title: 'test-title',
      content: { raw: 'test-description' },
    })
  })

  it('Should call Issue Show controller with correct params', () => {
    const execute = jest.fn()
    IssueShowController.mockImplementation(() => ({ execute }))

    new EditFormSaveController().execute(params)
    expect(IssueShowController).toBeCalled()
    expect(execute).toBeCalledWith({ id: 'id', owner: 'owner', repo: 'repo' })
  })
})
