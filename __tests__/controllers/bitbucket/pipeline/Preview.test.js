import Bitbucket from 'src/common/net/Bitbucket'
import PreviewController from 'src/controllers/bitbucket/pipeline/Preview'
import pipeline from '../../../helpers/mocks/responses/bitbucket/getPipeline.json'

jest.mock('src/common/net/Bitbucket')

const params = {
  owner: 'owner',
  repo: 'repo',
  id: 'id',
}

const getPipeline = jest.fn().mockReturnValue(pipeline)
Bitbucket.mockImplementation(() => ({ getPipeline }))

describe('Controller bitbucket.pipeline.Show', () => {
  beforeEach(() => Bitbucket.mockClear())

  it('Should call bitbucket', () => {
    new PreviewController().execute(params)

    expect(Bitbucket).toBeCalledWith(params.owner, params.repo)
    expect(getPipeline).toBeCalledWith(params.id, {
      fields: 'repository,target.commit.message,build_number,state,completed_on',
    })
  })

  it('Should return preview object', () => {
    expect(new PreviewController().execute(params)).toEqual({
      key: 'Pipeline #1123',
      source: 'test-repo',
      status: 'FAILED',
      title: 'target message',
      updatedAt: '2018-01-10T16:19:42.581Z',
    })
  })
})
