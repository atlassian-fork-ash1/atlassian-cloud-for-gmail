import Bitbucket from 'src/common/net/Bitbucket'
import PipelineShowController from 'src/controllers/bitbucket/pipeline/Show'
import PipelineView from 'src/views/bitbucket/Pipeline'

jest.mock('src/common/analytics')
jest.mock('src/common/net/Bitbucket')
jest.mock('src/views/bitbucket/Pipeline')

const params = {
  owner: 'owner',
  repo: 'repo',
  id: 'id',
}

const pipeline = Symbol('pipeline')

const getPipeline = jest.fn().mockReturnValue(pipeline)
Bitbucket.mockImplementation(() => ({ getPipeline }))

describe('Controller bitbucket.pipeline.Show', () => {
  beforeEach(() => {
    Bitbucket.mockClear()
    PipelineView.mockClear()
  })

  it('Returns pipeline view', () => {
    expect(new PipelineShowController().execute(params)).toBeInstanceOf(PipelineView)
  })

  it('Calls Bitbucket with correct params', () => {
    new PipelineShowController().execute(params)

    expect(Bitbucket).toBeCalledWith(params.owner, params.repo)
    expect(getPipeline).toBeCalledWith(params.id, {
      fields: '+repository.is_private,+repository.owner,+target.commit.message',
    })
  })

  it('Calls Pipeline view with correct params', () => {
    new PipelineShowController().execute(params)
    expect(PipelineView).toBeCalledWith({ pipeline })
  })
})
