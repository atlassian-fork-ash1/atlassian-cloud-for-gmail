import PrShowDescriptionController from 'src/controllers/bitbucket/pullRequest/ShowDescription'
import PrViewDescription from 'src/views/bitbucket/pullRequest/Description'

jest.mock('src/common/analytics')
jest.mock('src/views/bitbucket/pullRequest/Description')

const pullRequest = Symbol('pull request')

describe('Controller bitbucket.pullRequest.Show', () => {
  beforeEach(() => { PrViewDescription.mockClear() })

  it('Should return pull request view', () => {
    expect(new PrShowDescriptionController().execute({ pullRequest }))
      .toBeInstanceOf(PrViewDescription)
  })

  it('Should call PullRequest view with correct params', () => {
    new PrShowDescriptionController().execute({ pullRequest })
    expect(PrViewDescription).toBeCalledWith({ pullRequest })
  })
})
