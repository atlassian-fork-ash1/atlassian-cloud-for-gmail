import Bitbucket from 'src/common/net/Bitbucket'
import PullRequestShowController from 'src/controllers/bitbucket/pullRequest/Show'
import CommentReplyController from 'src/controllers/bitbucket/pullRequest/comment/Reply'
import pullRequest from '../../../../helpers/mocks/responses/bitbucket/getPullRequest.json'
import pullRequestComments from '../../../../helpers/mocks/responses/bitbucket/getPullRequestCommentsList.json'
import AddonError from '../../../../../src/common/AddonError'

jest.mock('src/common/analytics')
jest.mock('src/common/net/Bitbucket')
jest.mock('src/controllers/bitbucket/pullRequest/Show')

const execute = jest.fn()
const createPullRequestComment = jest.fn()

const { id, destination: { repository } } = pullRequest
const owner = repository.owner.username
const repo = repository.name

const params = {
  pullRequest,
  comment: pullRequestComments.values[0],
  content: 'lorem ipsum',
}

Bitbucket.mockImplementation(() => ({ createPullRequestComment }))
PullRequestShowController.mockImplementation(() => ({ execute }))

describe('Controller bitbucket.pullRequest.comment.Reply', () => {
  beforeEach(() => jest.clearAllMocks())

  it('Should call Bitbucket with correct params', () => {
    new CommentReplyController().execute(params)

    expect(Bitbucket).toBeCalledWith(owner, repo)
    expect(createPullRequestComment).toBeCalledWith(id, {
      content: params.content,
      parentCommentId: params.comment.id,
    })
  })

  it('Should call pullRequest.Show controller with correct params', () => {
    new CommentReplyController().execute(params)

    expect(PullRequestShowController).toBeCalled()
    expect(execute).toBeCalledWith({ owner, repo, id })
  })

  it('Should throw AddonError when no content provided', () => {
    const executeWithError = () => new CommentReplyController()
      .execute({ ...params, content: undefined })

    expect(executeWithError).toThrow(AddonError)
    expect(executeWithError).toThrowErrorMatchingSnapshot()
  })
})
