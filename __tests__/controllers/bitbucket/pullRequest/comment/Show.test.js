import CommentShowController from 'src/controllers/bitbucket/pullRequest/comment/Show'
import PullRequestCommentView from 'src/views/bitbucket/pullRequest/Comment'

jest.mock('src/views/bitbucket/pullRequest/Comment')

const params = {
  pullRequest: 'pull request',
  comment: 'comment',
}

describe('Controller bitbucket.comment.Show', () => {
  beforeEach(() => jest.clearAllMocks())

  it('Should return comment view', () => {
    expect(new CommentShowController().execute(params))
      .toBeInstanceOf(PullRequestCommentView)
  })

  it('Show call bitbucket.pullRequest.Comment view with correct parameters', () => {
    new CommentShowController().execute(params)

    expect(PullRequestCommentView).toBeCalledWith(params)
  })
})
