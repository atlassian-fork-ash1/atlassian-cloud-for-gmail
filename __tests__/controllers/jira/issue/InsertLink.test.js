import InsertLink from 'src/controllers/jira/issue/InsertLink'
import Jira from 'src/common/net/Jira'

jest.mock('src/common/net/Jira')

const getServerInfo = jest.fn().mockReturnValue({ baseUrl: 'https://test.com' })
Jira.mockImplementation(() => ({ getServerInfo }))

const issue = {
  self: 'https://api.atlassian.com/ex/jira/test-cloud-id/rest/api/2/issue/10013',
  key: 'TEST-1337',
  fields: {
    summary: 'lorem ipsum',
    project: { name: 'TEST-PROJECT' },
  },
}

describe('Controller jira.issue.InsertLink', () => {
  it('Should call getServerInfo', () => {
    new InsertLink().execute({ issue })
    expect(getServerInfo).toBeCalledWith({ cloudId: 'test-cloud-id' })
  })

  it('Should return correct html', () => {
    expect(new InsertLink().execute({ issue, icon: 'dummy-icon' })).toMatchSnapshot()
  })
})
