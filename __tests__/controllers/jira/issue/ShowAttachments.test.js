import IssueAttachmentShowController from 'src/controllers/jira/issue/ShowAttachments'
import Jira from 'src/common/net/Jira'
import IssueAttachmentsView from 'src/views/jira/issue/Attachments'

jest.mock('src/common/analytics')
jest.mock('src/common/net/Jira')
jest.mock('src/views/jira/issue/Attachments')

const params = {
  instanceHost: 'dummy-domain.atlassian.net',
  issueKey: 'DEV-1337',
}

const issueResponse = {
  key: params.issueKey,
  fields: { summary: 'bar', attachment: 'foo' },
}

const getIssue = jest.fn().mockReturnValue(issueResponse)
Jira.mockImplementation(() => ({ getIssue }))

describe('Controller jira.Issue.ShowAttachment', () => {
  beforeEach(() => jest.clearAllMocks())

  it('Should return issue attachment view', () => {
    expect(new IssueAttachmentShowController().execute(params)).toBeInstanceOf(IssueAttachmentsView)
  })

  it('Should call Jira with correct params', () => {
    new IssueAttachmentShowController().execute(params)

    expect(Jira).toBeCalledWith(params.instanceHost)
    expect(getIssue).toBeCalledWith(params.issueKey, {
      fields: 'summary,attachment',
    })
  })

  it('Should call Issue  attachment view with correct params', () => {
    new IssueAttachmentShowController().execute(params)
    expect(IssueAttachmentsView).toBeCalledWith({
      issue: issueResponse,
      instanceHost: params.instanceHost,
    })
  })
})
