import AssigneeSaveController from 'src/controllers/jira/issue/assignee/Save'
import Jira from 'src/common/net/Jira'
import AddonError from 'src/common/AddonError'

jest.mock('src/common/net/Jira')
jest.mock('src/common/analytics')
jest.mock('src/common/AddonError')

const params = {
  instanceHost: 'dummy-domain.atlassian.net',
  issueKey: 'DEV-123',
  assigneeEmail: 'johndoe@mail.com',
}

const issue = {
  editmeta: {
    fields: {
      priorities: Symbol('priorities'),
      assignee: Symbol('assignee'),
      description: Symbol('description'),
    },
  },
}
const issueComments = {
  comments: Symbol('comments'),
  total: 1337,
}
const user = {
  account_id: Symbol('account_id'),
}

const assignIssue = jest.fn().mockReturnThis()
const searchUsers = jest.fn()
const getIssue = jest.fn().mockReturnThis()
const getIssueComments = jest.fn().mockReturnThis()
const getCurrentUser = jest.fn().mockReturnThis()
const getPriorities = jest.fn().mockReturnThis()
const parallel = jest.fn().mockReturnThis()
const fetch = jest.fn().mockReturnValue([issue, issueComments, user])

Jira.mockImplementation(() => ({
  getIssue,
  getIssueComments,
  getCurrentUser,
  getPriorities,
  assignIssue,
  searchUsers,
  parallel,
  fetch,
}))

describe('Controller jira.issue.assignee.Save', () => {
  beforeEach(() => {
    Jira.mockClear()
  })

  it('Should call Jira with correct params', () => {
    searchUsers.mockReturnValueOnce([{ name: '~admin' }])
    new AssigneeSaveController().execute(params)

    expect(Jira).toBeCalledWith(params.instanceHost)
    expect(searchUsers).toBeCalledWith({ username: params.assigneeEmail })
    expect(assignIssue).toBeCalledWith(params.issueKey, { name: '~admin' })
  })

  it('Should unassign an issue', () => {
    new AssigneeSaveController().execute({ ...params, assigneeEmail: 'Unassigned' })

    expect(assignIssue).toBeCalledWith(params.issueKey, { name: null })
  })

  it('Should throw an Addon Error', () => {
    searchUsers.mockReturnValueOnce([])
    const execute = () => new AssigneeSaveController().execute(params)

    expect(execute).toThrow(AddonError)
  })
})
