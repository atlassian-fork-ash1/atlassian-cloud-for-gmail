import AssigneeShowController from 'src/controllers/jira/issue/assignee/Show'
import AssigneeView from 'src/views/jira/issue/Assignee'

const params = {
  instanceHost: 'dummy-domain.atlassian.net',
  issue: Symbol('issue'),
}

describe('Controller jira.Issue.assignee.Show', () => {
  it('Should return Assignee view', () => {
    expect(new AssigneeShowController().execute(params)).toBeInstanceOf(AssigneeView)
  })
})
