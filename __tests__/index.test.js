import actions from 'src/controllers'
import { dispatchAction } from 'src/'

const mockRun = jest.fn()
jest.mock('../src/controllers', () => ({
  foo: {
    Bar: jest.fn().mockImplementation(() => ({ run: mockRun })),
  },
}))

describe('Entry Points', () => {
  test('dispatchAction calls correct action with correct params', () => {
    const event = {
      parameters: {
        actionName: 'foo.Bar',
        args: JSON.stringify({
          param1: 'param1',
          param2: 'param2',
        }),
      },
      formInput: {
        input1: 'input1',
        input2: 'input2',
      },
    }

    dispatchAction(event)

    expect(actions.foo.Bar).toBeCalledWith({ event })
    expect(mockRun).toBeCalledWith({
      param1: 'param1',
      param2: 'param2',
      input1: 'input1',
      input2: 'input2',
    })
  })
})
