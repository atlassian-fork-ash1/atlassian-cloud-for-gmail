import BlankView from 'src/views/addon/Blank'

describe('View Blank', () => {
  it('Renders card', () => {
    const card = new BlankView().render()
    expect(card.printJson()).toMatchSnapshot()
  })
})
