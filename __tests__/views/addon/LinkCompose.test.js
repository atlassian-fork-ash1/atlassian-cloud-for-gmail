import LinkComposeView from 'src/views/addon/LinkCompose'
import issue from '../../helpers/mocks/responses/jira/getIssue.json'

const issues = [issue, issue]

describe('View LinkCompose', () => {
  it('Renders card', () => {
    const card = new LinkComposeView({ issues }).render()
    expect(card.printJson()).toMatchSnapshot()
  })
})
