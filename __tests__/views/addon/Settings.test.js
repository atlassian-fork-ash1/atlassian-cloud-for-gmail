import SettingsView from 'src/views/addon/Settings'
import bitbucketUser from '../../helpers/mocks/responses/bitbucket/getCurrentUser.json'
import jiraUser from '../../helpers/mocks/responses/jira/getCurrentUser.json'

const bitbucketAuthUrl = 'https://bitbucket-auth.com'
const jiraAuthUrl = 'https://jira-auth.com'
const jiraSites = [
  { baseUrl: 'https://test.atlassian.net' },
  { baseUrl: 'https://test2.atlassian.net' },
]
const email = [
  {
    email: 'test@atlassian.com',
    is_confirmed: true,
    is_primary: true,
    type: 'email',
  },
]

describe('View Settings', () => {
  it('Renders view with authorized users', () => {
    const card = new SettingsView({
      jiraAuthUrl,
      bitbucketAuthUrl,
      bitbucketUser: { ...bitbucketUser, emails: email },
      jiraUser: { ...jiraUser, sites: jiraSites },
    }).render()
    expect(card.printJson()).toMatchSnapshot()
  })

  it('Renders view without authorized users', () => {
    const card = new SettingsView({
      jiraAuthUrl,
      bitbucketAuthUrl,
    }).render()
    expect(card.printJson()).toMatchSnapshot()
  })
})
