import { Card } from 'gas-mock-service'
import FeedbackView from 'src/views/addon/feedback/Feedback'

const currentUserId = Symbol('currentUserId')

describe('Feedback view', () => {
  it('Runs the render method and checks the returned data is a card instance', () => {
    const card = new FeedbackView({ currentUserId }).render()

    expect(card).toBeInstanceOf(Card)
    expect(card.printJson()).toMatchSnapshot()
  })

  it('Runs the render method and prints a card correctly when no `currentUserId` provided', () => {
    const card = new FeedbackView({}).render()

    expect(card).toBeInstanceOf(Card)
    expect(card.printJson()).toMatchSnapshot()
  })
})
