import { Card } from 'gas-mock-service'
import FeedbackSuccess from 'src/views/addon/feedback/Success'

describe('Feedback view', () => {
  it('Runs the render method and checks the returned data is a card instance', () => {
    const card = new FeedbackSuccess().render()

    expect(card).toBeInstanceOf(Card)
    expect(card.printJson()).toMatchSnapshot()
  })
})
