import { Card } from 'gas-mock-service'
import JiraOnboardingView from 'src/views/addon/onboarding/Jira'

describe('Bitbucket onboarding view', () => {
  it('Renders Bitbucket onboarding card', () => {
    const card = new JiraOnboardingView({ authUrl: 'authUrl' }).render()

    expect(card).toBeInstanceOf(Card)
    expect(card.printJson()).toMatchSnapshot()
  })
})
