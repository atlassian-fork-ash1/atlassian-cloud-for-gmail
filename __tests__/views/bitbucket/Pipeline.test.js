import { Card } from 'gas-mock-service'
import { cloneDeep } from 'lodash'
import PipelineView from 'src/views/bitbucket/Pipeline'
import pipeline from '../../helpers/mocks/responses/bitbucket/getPipeline.json'

describe('View Pipeline', () => {
  it('Runs the render method and checks the returned data is a card instance', () => {
    const card = new PipelineView({ pipeline }).render()

    expect(card).toBeInstanceOf(Card)
  })

  describe('Runs the render method and checks the data of card service is correct', () => {
    it('Data with failed build', () => {
      const card = new PipelineView({ pipeline }).render()

      expect(card.printJson()).toMatchSnapshot()
    })

    it('Data with  successful build', () => {
      const pipelineSuccessful = cloneDeep(pipeline)
      pipelineSuccessful.state.result = { type: 'pipeline_state_completed_successful', name: 'SUCCESSFUL' }
      const card = new PipelineView({ pipeline: pipelineSuccessful }).render()

      expect(card.printJson()).toMatchSnapshot()
    })
  })
})
