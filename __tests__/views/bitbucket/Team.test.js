import { Card } from 'gas-mock-service'
import TeamView from 'src/views/bitbucket/Team'
import team from '../../helpers/mocks/responses/bitbucket/getTeam.json'

describe('View Team', () => {
  it('Runs the render method and checks the data of used services', () => {
    const card = new TeamView({ team }).render()

    expect(card).toBeInstanceOf(Card)
    expect(card.printJson()).toMatchSnapshot()
  })
})
