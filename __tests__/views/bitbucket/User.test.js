import { Card } from 'gas-mock-service'
import UserView from 'src/views/bitbucket/User'
import user from '../../helpers/mocks/responses/bitbucket/getUser.json'

describe('View bitbucket.user.Show', () => {
  it('Runs the render method and checks the returned data is a card instance', () => {
    const card = new UserView({ user }).render()

    expect(card).toBeInstanceOf(Card)
  })

  it('Runs the render method and checks the data of card service is correct', () => {
    const card = new UserView({ user }).render()

    expect(card.printJson()).toMatchSnapshot()
  })
})
