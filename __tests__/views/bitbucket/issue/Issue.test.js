import { Card } from 'gas-mock-service'
import { cloneDeep } from 'lodash'
import IssueView from 'src/views/bitbucket/issue/Issue'
import issue from '../../../helpers/mocks/responses/bitbucket/getIssue.json'
import comments from '../../../helpers/mocks/responses/bitbucket/getIssueComments.json'
import attachments from '../../../helpers/mocks/responses/bitbucket/getIssueAttachments.json'

const emptyData = {
  pagelen: 10,
  values: [],
  page: 1,
  size: 0,
}

describe('View Issue', () => {
  it('Runs the render method and checks the data of used services when issue comments exist', () => {
    const card = new IssueView({ issue, comments, attachments, owner: 'owner' }).render()

    expect(card).toBeInstanceOf(Card)
    expect(card.printJson()).toMatchSnapshot()
  })

  it('Runs the render method and checks the data of used services when issue comments do not exist', () => {
    const card = new IssueView({ issue, comments: emptyData, attachments, owner: 'owner' }).render()

    expect(card).toBeInstanceOf(Card)
    expect(card.printJson()).toMatchSnapshot()
  })

  it('Runs the render method and checks the data of used services when no attachments provided', () => {
    const card = new IssueView({ issue, comments, attachmentsSize: 0, owner: 'owner' }).render()

    expect(card).toBeInstanceOf(Card)
    expect(card.printJson()).toMatchSnapshot()
  })

  it('Runs the render method and checks the data of used services when no access to attachments', () => {
    const card = new IssueView({ issue, comments, owner: 'owner' }).render()

    expect(card).toBeInstanceOf(Card)
    expect(card.printJson()).toMatchSnapshot()
  })

  it('Runs the render method and checks the data of used services when no issue decription provided', () => {
    const clonedIssue = cloneDeep(issue)
    clonedIssue.content.html = ''
    const card = new IssueView({ issue: clonedIssue, comments, attachments: emptyData, owner: 'owner' }).render()

    expect(card).toBeInstanceOf(Card)
    expect(card.printJson()).toMatchSnapshot()
  })
})
