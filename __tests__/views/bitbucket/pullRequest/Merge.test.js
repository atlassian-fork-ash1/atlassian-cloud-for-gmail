import { Card } from 'gas-mock-service'
import MergeView from 'src/views/bitbucket/pullRequest/Merge'
import pullRequest from '../../../helpers/mocks/responses/bitbucket/getPullRequest.json'

describe('View PullRequest Merge', () => {
  it('Runs the render method and checks the data of used services', () => {
    const card = new MergeView({ pullRequest }).render()

    expect(card).toBeInstanceOf(Card)
    expect(card.printJson()).toMatchSnapshot()
  })
})
