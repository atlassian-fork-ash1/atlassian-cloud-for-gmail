import IssueEditFormView from 'src/views/jira/issue/EditForm'
import issue from '../../../helpers/mocks/responses/jira/getIssueWithTransitions.json'
import priorities from '../../../helpers/mocks/responses/jira/getPriorities.json'

const params = {
  instanceHost: 'main-jira-in-the-w0rld.atlassian.net',
  editableFields: ['priority', 'status', 'assignee', 'summary', 'description'],
  issue,
  priorities,
}

describe('View Issue/EditForm', () => {
  it('Should check that the issue editForm card data is correct', () => {
    const card = new IssueEditFormView(params).render()

    expect(card.printJson()).toMatchSnapshot()
  })
})
