import { Card } from 'gas-mock-service'
import { cloneDeep } from 'lodash'
import IssueView from 'src/views/jira/issue/Issue'
import issue from '../../../helpers/mocks/responses/jira/getIssue.json'
import issueComments from '../../../helpers/mocks/responses/jira/getComments.json'
import currentUser from '../../../helpers/mocks/responses/jira/getCurrentUser.json'

const params = {
  instanceHost: 'main-jira-in-the-w0rld.atlassian.net',
  issue,
  issueComments,
  currentUser,
  editableFields: Object.keys(issue.editmeta.fields),
}

describe('View Issue', () => {
  it('Should run the render method and check the returned data is a card instance', () => {
    const card = new IssueView(params).render()

    expect(card).toBeInstanceOf(Card)
  })

  it('Should check that the issue card data is correct', () => {
    const card = new IssueView(params).render()

    expect(card.printJson()).toMatchSnapshot()
  })

  it('Should check that the priority icon is external', () => {
    const priorityCustomIconUrl = 'https://external-domain.com/custom-icon.svg'
    const updatedIssue = cloneDeep(issue)

    updatedIssue.fields.priority.iconUrl = priorityCustomIconUrl

    const card = new IssueView({ ...params, issue: updatedIssue }).render()

    expect(card.printJson()).toMatchSnapshot()
  })

  it('Should render issue card when issue doesn\'t have labels/components properties', () => {
    const updatedIssue = cloneDeep(issue)
    delete updatedIssue.fields.labels
    delete updatedIssue.fields.components

    expect(new IssueView({ ...params, issue: updatedIssue }).render()).toMatchSnapshot()
  })

  it('Should render issue card correctly when no attachments provided', () => {
    const updatedIssue = cloneDeep(issue)
    updatedIssue.fields.attachment = []

    expect(new IssueView({ ...params, issue: updatedIssue }).render()).toMatchSnapshot()
  })
})
