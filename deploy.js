/* eslint-disable import/no-extraneous-dependencies */
const fs = require('fs')
const shell = require('shelljs')
const moment = require('moment')

const { NODE_ENV, SCRIPT_ID, DEPLOYMENT_ID, REFRESH_TOKEN, BITBUCKET_COMMIT } = process.env

run()

async function run () {
  if (!NODE_ENV || !SCRIPT_ID || !REFRESH_TOKEN) {
    console.error('Required environment variables was not provided.')
    process.exit(1)
  }

  try {
    initClasp()

    build()
    push()

    if (DEPLOYMENT_ID) {
      await deploy()
    }
  } catch (error) {
    console.error(error.errors || error)
    process.exit(1)
  }
}

function initClasp () {
  const credentials = {
    token: {
      refresh_token: REFRESH_TOKEN,
      token_type: 'Bearer',
    },
  }

  // write credentials to file so clasp could use them
  fs.writeFileSync(`${__dirname}/.clasprc.json`, JSON.stringify(credentials))

  // write script id to file so clasp could use it
  fs.writeFileSync(`${__dirname}/.clasp.json`, JSON.stringify({ scriptId: SCRIPT_ID }))
}

function build () {
  shell.exec('npm run build')
}

function push () {
  shell.exec('./node_modules/.bin/clasp push -f')
}

async function deploy () {
  const description = `${moment().format('YYYY_MM_DD_HH-mm')}_${BITBUCKET_COMMIT}`

  shell.exec(`./node_modules/.bin/clasp deploy --deploymentId ${DEPLOYMENT_ID} --description ${description}`)
}
