// There is no way to `extend` built-in classes such as Error in GAS
// so it's a workaround to have custom error class
export default function AddonError (message, code, product) {
  Error.call(this, message)
  this.message = message
  this.code = code
  this.product = product
}

AddonError.prototype = Object.create(Error.prototype)
