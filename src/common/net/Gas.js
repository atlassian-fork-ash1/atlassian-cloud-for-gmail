import HttpClient from './HttpClient'
import { formatUrl } from '../utils'

export default class Gas {
  constructor () {
    this._httpClient = new HttpClient()
  }

  send (body) {
    return this.fetch(
      { pathname: '/v1/event' },
      { method: 'POST', body }
    )
  }

  fetch ({ pathname, query = {} }, { method, body, headers = {} } = {}) {
    const url = formatUrl({
      host: 'https://mgas.prod.public.atl-paas.net',
      pathname,
      query,
    })

    this._httpClient.enqueueRequest(url, { method, body, headers })

    this._httpClient.execute()
  }
}
