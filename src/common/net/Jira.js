import HttpStatus from 'http-status-codes'
import { formatUrl } from '../utils'
import { scriptProperties } from '../properties'
import AddonError from '../AddonError'
import HttpClient from './HttpClient'

export default class Jira {
  constructor (instanceHost, cloudId) {
    this._instanceHost = instanceHost
    this._instanceBaseUrl = `https://${this._instanceHost}`
    this._cloudId = cloudId
    this._httpClient = new HttpClient()
  }

  static oauthService () {
    const credentials = scriptProperties.get('atlassianCredentials')
    const redirectUri = `https://script.google.com/macros/d/${ScriptApp.getScriptId()}/usercallback`

    return OAuth2.createService('atlassianApp')
      .setAuthorizationBaseUrl('https://accounts.atlassian.com/authorize')
      .setTokenUrl('https://accounts.atlassian.com/oauth/token')
      .setScope('offline_access read:jira-user read:jira-work write:jira-work')
      .setParam('redirect_uri', redirectUri)
      .setParam('response_type', 'code')
      .setParam('audience', 'api.atlassian.com')
      .setParam('prompt', 'consent')
      .setClientId(credentials.clientId)
      .setClientSecret(credentials.clientSecret)
      .setCallbackFunction('jiraOAuthCallback')
      .setPropertyStore(PropertiesService.getUserProperties())
      .setCache(CacheService.getUserCache())
  }

  parallel () {
    this._httpClient.setParallel(true)
    return this
  }

  getCurrentUser () {
    return this._enqueue({ pathname: '/me', resourceScoped: false })
  }

  searchIssues (query, { cloudId } = {}) {
    return this._enqueue({ pathname: '/rest/api/2/search', query, cloudId })
  }

  getIssue (id, query) {
    return this._enqueue({ pathname: `/rest/api/2/issue/${id}`, query })
  }

  updateIssue (id, body) {
    return this._enqueue(
      { pathname: `/rest/api/2/issue/${id}` },
      { method: 'PUT', body },
    )
  }

  createIssueTransition (id, body) {
    return this._enqueue(
      { pathname: `/rest/api/2/issue/${id}/transitions` },
      { method: 'POST', body },
    )
  }

  addWatcher (id, body) {
    return this._enqueue(
      { pathname: `/rest/api/2/issue/${id}/watchers` },
      { method: 'POST', body },
    )
  }

  removeWatcher (id, query) {
    return this._enqueue(
      { pathname: `/rest/api/2/issue/${id}/watchers`, query },
      { method: 'DELETE' }
    )
  }

  getIssueComments (id, query) {
    return this._enqueue({ pathname: `/rest/api/2/issue/${id}/comment`, query })
  }

  createIssueComment (id, body) {
    return this._enqueue({ pathname: `/rest/api/2/issue/${id}/comment` }, { body })
  }

  getUser (query) {
    return this._enqueue({ pathname: '/rest/api/2/user', query })
  }

  searchUsers (query) {
    return this._enqueue({ pathname: '/rest/api/2/user/search', query })
  }

  searchAssignableUsers (query) {
    return this._enqueue({ pathname: '/rest/api/2/user/assignable/search', query })
  }

  assignIssue (id, body) {
    return this._enqueue(
      { pathname: `/rest/api/2/issue/${id}/assignee` },
      { method: 'PUT', body }
    )
  }

  getPriorities () {
    return this._enqueue({ pathname: '/rest/api/2/priority' })
  }

  getProjects () {
    return this._enqueue({ pathname: '/rest/api/2/project' })
  }

  getAccessibleResources () {
    return this._enqueue({ pathname: '/oauth/token/accessible-resources', resourceScoped: false })
  }

  getServerInfo ({ cloudId } = {}) {
    return this._enqueue({ pathname: `/rest/api/2/serverInfo`, cloudId })
  }

  fetch () {
    try {
      const response = this._httpClient.execute()

      return this._replaceDomainUrls(response)
    } catch (error) {
      if (!error.response) throw error

      const { statusCode, body } = error.response
      const message = body.message || ''

      if (message === 'Unauthorized' || this._isStargateUnauthorizedResponse(error.response)) {
        throw new AddonError('Authorization required', 'UNAUTHORIZED', 'JIRA')
      }

      if (statusCode === HttpStatus.FORBIDDEN) {
        throw new AddonError('You have no access to this resource', 'FORBIDDEN', 'JIRA')
      }

      if (statusCode === HttpStatus.NOT_FOUND) {
        throw new AddonError('Object not found in Jira', 'NOT_FOUND', 'JIRA')
      }

      throw error
    }
  }

  /**
  * Icons, mention links etc, accessible only through the URL based on current instance domain
  * The /ex/jira/${cloud_id}/ segments should be removed from the path
  *
  * But we'll have to be careful if we're following any restful hypermedia API links
  *  (self links etc) not to transform those. (e.g. https://api.atlassian.com/ex/jira/${cloud_id}/rest/etc)
  * We could just avoid transforming anything under /rest/
  * This additional regexp check is dedicated to skip the substitution:
  *   match.includes('/rest/')
  */
  _replaceDomainUrls (response) {
    if (response && this._instanceHost) {
      const regex = /https:\/\/api\.atlassian\.com\/ex\/jira\/[^/]+\/([^\s",]+)/g

      response = JSON.stringify(response)
      response = response
        .replace(regex, (match, query) => (match.includes('/rest/') ? match : `${this._instanceBaseUrl}/${query}`))
        // It's not allowed to have empty <a> tags in gmail addons so
        // here's a workaround for cases when we get empty <a> tag from API.
        // Applies for issue attachments.
        .replace(/href=\\"\/secure\//g, `href=\\"${this._instanceBaseUrl}/secure/`)

      response = JSON.parse(response)
    }

    return response
  }

  /**
   * If you make a request to:
   *
   * https://api.atlassian.com/ex/jira/${cloud_id}/rest/api/latest/${issue_key}
   *
   * And you get a 404, it could mean two different things:
   *
   * - You haven’t authorized the app for access to ${cloud_id}
   * - ${issue_key} does not exist
   *
   * Since the user can deauthorize/reauthorize the app at any time, this is a
   * bit of a problem, as you can never really tell if an app has lost access
   * or the issue simply doesn’t exist.
   *
   * At the moment the response from Stargate is pretty generic:
   *
   * {code: 404, message: "Not Found"}
   *
   * But fortunately Jira doesn't seem to return messages matching this format
   * for missing resources. Hence the sketchy test below.
   */
  _isStargateUnauthorizedResponse ({ statusCode, body }) {
    return statusCode === 404 && body.message === 'Not Found'
  }

  _enqueue (
    { pathname, query = {}, cloudId = this._cloudId, resourceScoped = true },
    { method, body, headers = {} } = {}
  ) {
    const oauthService = Jira.oauthService()

    if (!oauthService.hasAccess()) {
      throw new AddonError('Authorization required', 'UNAUTHORIZED', 'JIRA')
    }

    if (resourceScoped) {
      pathname = `/ex/jira/${cloudId || this._lookupCloudId()}${pathname}`
    }

    const url = formatUrl({
      host: `https://api.atlassian.com`,
      pathname,
      query,
    })

    if (headers.Authorization === undefined) {
      headers.Authorization = `Bearer ${oauthService.getAccessToken()}`
    }

    this._httpClient.enqueueRequest(url, { method, body, headers })

    if (this._httpClient.isParallel()) {
      return this
    }

    return this.fetch()
  }

  _lookupCloudId () {
    const jira = new Jira()
    const accessibleResources = jira.getAccessibleResources()

    // get all servers info in a parallel
    jira.parallel()
    accessibleResources.forEach(resource => jira.getServerInfo({ cloudId: resource.id }))
    const serversInfo = jira.fetch()

    const resource = accessibleResources.find((r, i) => {
      const serverInfoBaseUrl = serversInfo[i].baseUrl

      return this._instanceBaseUrl === serverInfoBaseUrl
    })

    if (!resource) {
      throw new AddonError(`Token not authorized for ${this._instanceHost}`, 'UNAUTHORIZED', 'JIRA')
    }

    this._cloudId = resource.id
    return resource.id
  }
}
