import querystring from 'querystring'
import encodeUrl from 'encodeurl'
import { isArray, isObject, isString } from 'lodash'

const secretFields = [
  'secret',
  'token',
  'oauth',
  'publicKey',
  'body',
  'text',
  'description',
  'customfield_',
  'displayName',
  'userKey',
  'username',
  'Authorization',
]

export function formatUrl ({ host, pathname, query }) {
  if (pathname[0] !== '/') {
    pathname = `/${pathname}`
  }

  if (typeof query === 'object') {
    query = `?${querystring.stringify(query)}`
  }

  return encodeUrl(`${host}${pathname}${query}`)
}

export function getUrls (string) {
  const expression = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,4}\b([-a-zA-Z0-9{}@:%_+.~!#?&//=]*)/g

  return string.match(new RegExp(expression)) || []
}

export function copyWithoutSecrets (data) {
  if (isArray(data)) {
    return data.map(element => copyWithoutSecrets(element))
  }

  if (isObject(data)) {
    return Object.keys(data).reduce((acc, key) => {
      const skip = secretFields
        .some(secretField => key.toLowerCase().includes(secretField.toLowerCase()))

      acc[key] = skip && isString(data[key])
        ? '<MASKED>'
        : copyWithoutSecrets(data[key])

      return acc
    }, {})
  }

  return data
}

export function pickBy (object, predicate) {
  return Object.entries(object)
    .reduce((result, [key, value]) => {
      if (predicate(value)) result[key] = value
      return result
    }, {})
}
