import Controller from './Controller'
import AlertView from '../views/addon/Alert'
import { getNavigationActionResponse } from './responseWrappers'

/**
 * Replaces current card in place
 * with a new card returned from execute
 */
export default class extends Controller {
  navigateTo (view) {
    const card = view.render()

    // alerts should be pushed to navigation stack to have `back` button in header
    return view instanceof AlertView
      ? getNavigationActionResponse(card)
      : card
  }
}
