import Bitbucket from '../common/net/Bitbucket'
import Jira from '../common/net/Jira'
import AddonError from '../common/AddonError'
import analytics from '../common/analytics'
import { copyWithoutSecrets } from '../common/utils'
import AlertView from '../views/addon/Alert'

export default class Controller {
  constructor ({ event } = {}) {
    this.event = event
    this.messageMetadata = event && event.messageMetadata
  }

  run (...args) {
    try {
      analytics.init(Session.getEffectiveUser().getEmail())
      return this.navigateTo(this.execute(...args))
    } catch (error) {
      if (error instanceof AddonError) {
        if (error.code === 'UNAUTHORIZED') {
          return this._handleAuthorizationRequiredError(error.product)
        }

        analytics.pageView('Warning')

        return this.navigateTo(new AlertView({
          text: error.message,
          type: 'warning',
        }))
      }

      analytics.pageView('Error')
      console.error({
        message: error.message,
        ...copyWithoutSecrets(error),
      })

      return this.navigateTo(new AlertView({
        text: 'Something went wrong!',
        type: 'error',
      }))
    } finally {
      analytics.flush()
    }
  }

  _handleAuthorizationRequiredError (product) {
    switch (product) {
    case 'BITBUCKET':
      return CardService.newAuthorizationException()
        .setAuthorizationUrl(Bitbucket.oauthService().getAuthorizationUrl())
        .setResourceDisplayName('Bitbucket')
        .throwException()
    case 'JIRA':
      return CardService.newAuthorizationException()
        .setAuthorizationUrl(Jira.oauthService().getAuthorizationUrl())
        .setResourceDisplayName('Jira')
        .throwException()
    default:
      throw new AddonError(`Unrecognized product ${product}`, 'UNKNOWN_ERROR')
    }
  }
}
