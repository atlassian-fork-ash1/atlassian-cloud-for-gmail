import Controller from './Controller'
import { getSuggestionResponse } from './responseWrappers'

export default class extends Controller {
  navigateTo (suggestions) {
    return getSuggestionResponse(suggestions)
  }
}
