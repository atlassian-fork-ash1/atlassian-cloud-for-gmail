import Controller from './Controller'
import { getUniversalActionResponse } from './responseWrappers'

export default class extends Controller {
  navigateTo (view) {
    return getUniversalActionResponse(view.render())
  }
}
