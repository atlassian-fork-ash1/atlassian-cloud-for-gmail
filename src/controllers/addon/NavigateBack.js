import Controller from '../Controller'

export default class extends Controller {
  execute () {
    // nothing to execute here
  }

  navigateTo () {
    const nav = CardService.newNavigation().popCard()

    return CardService.newActionResponseBuilder()
      .setNavigation(nav)
      .build()
  }
}
