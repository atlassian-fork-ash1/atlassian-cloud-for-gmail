import { get } from 'lodash'
import UniversalActionController from '../../UniversalActionController'
import analytics from '../../../common/analytics'
import FeedbackView from '../../../views/addon/feedback/Feedback'
import Jira from '../../../common/net/Jira'
import Bitbucket from '../../../common/net/Bitbucket'

export default class extends UniversalActionController {
  execute () {
    analytics.pageView('Feedback')

    const currentUserId = this._getCurrentUserAccountIdByProduct('jira') ||
      this._getCurrentUserAccountIdByProduct('bitbucket')

    return new FeedbackView({ currentUserId })
  }

  _getCurrentUserAccountIdByProduct (product) {
    const products = {
      jira: new Jira(),
      bitbucket: new Bitbucket(),
    }

    try {
      return get(products[product].getCurrentUser(), 'account_id')
    } catch (error) {
      // If user didn't login neither in Jira nor in Bitbucket currentUserId
      // will turn into falsy value and will eventually turn into user: '-'.
      if (error.code === 'UNAUTHORIZED') return

      throw error
    }
  }
}
