import ActionPushController from '../../ActionPushController'
import IssueDescriptionView from '../../../views/bitbucket/issue/Description'
import analytics from '../../../common/analytics'

export default class Show extends ActionPushController {
  execute ({ issue }) {
    analytics.event({
      category: 'Bitbucket Issue',
      action: 'Show description',
    })

    return new IssueDescriptionView({ issue })
  }
}
