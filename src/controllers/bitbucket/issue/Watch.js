import ActionController from '../../ActionController'
import Bitbucket from '../../../common/net/Bitbucket'
import ShowController from './Show'
import analytics from '../../../common/analytics'

export default class extends ActionController {
  execute ({ owner, repo, id, watch }) {
    new Bitbucket(owner, repo).watchIssue(id, watch)

    analytics.event({
      category: 'Bitbucket Issue',
      action: watch ? 'Watch' : 'Stop watching',
    })

    return new ShowController({ event: this.event }).execute({ owner, repo, id })
  }
}
