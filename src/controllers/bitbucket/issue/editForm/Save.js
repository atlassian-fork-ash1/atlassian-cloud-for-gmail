import ActionPopController from '../../../ActionPopController'
import Bitbucket from '../../../../common/net/Bitbucket'
import analytics from '../../../../common/analytics'
import ShowIssue from '../Show'

export default class extends ActionPopController {
  execute ({ owner, repo, id, title, priority, kind, content }) {
    new Bitbucket(owner, repo).updateIssue(id, {
      title,
      priority,
      kind,
      content: { raw: content },
    })

    analytics.event({
      category: 'Bitbucket Issue',
      action: 'Edit',
    })

    return new ShowIssue({ event: this.event }).execute({ owner, repo, id })
  }
}
