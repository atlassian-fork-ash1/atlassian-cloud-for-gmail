import { get } from 'lodash'
import ActionPushController from '../../ActionPushController'
import Bitbucket from '../../../common/net/Bitbucket'

export default class extends ActionPushController {
  execute ({ owner, repo, id }) {
    const fields = [
      'repository',
      'target.commit.message',
      'build_number',
      'state',
      'completed_on',
    ].join(',')

    const pipeline = new Bitbucket(owner, repo).getPipeline(id, { fields })

    return {
      source: pipeline.repository.name,
      title: pipeline.target.commit.message.trim(),
      key: `Pipeline #${pipeline.build_number}`,
      status: get(pipeline, 'state.result.name') || pipeline.state.name,
      updatedAt: pipeline.completed_on,
    }
  }
}
