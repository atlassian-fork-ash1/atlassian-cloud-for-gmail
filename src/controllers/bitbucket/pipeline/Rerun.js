import ActionController from '../../ActionController'
import Bitbucket from '../../../common/net/Bitbucket'
import ShowController from './Show'
import analytics from '../../../common/analytics'

export default class extends ActionController {
  execute ({ owner, repo, commitHash, branchName }) {
    const createdPipeline = new Bitbucket(owner, repo).rerunPipeline(commitHash, branchName)

    analytics.event({
      category: 'Bitbucket Pipeline',
      action: 'Rerun',
    })

    return new ShowController({ event: this.event })
      .execute({ owner, repo, id: createdPipeline.uuid })
  }
}
