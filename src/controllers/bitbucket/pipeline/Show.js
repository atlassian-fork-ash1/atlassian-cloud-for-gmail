import ActionPushController from '../../ActionPushController'
import Bitbucket from '../../../common/net/Bitbucket'
import PipelineView from '../../../views/bitbucket/Pipeline'
import analytics from '../../../common/analytics'

export default class Show extends ActionPushController {
  execute ({ owner, repo, id }) {
    const fields = [
      '+repository.is_private',
      '+repository.owner',
      '+target.commit.message',
    ].join(',')

    const pipeline = new Bitbucket(owner, repo).getPipeline(id, { fields })

    analytics.pageView('Bitbucket Pipeline')

    return new PipelineView({ pipeline })
  }
}
