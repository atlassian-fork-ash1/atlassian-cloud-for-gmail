import ActionPushController from '../../ActionPushController'
import Bitbucket from '../../../common/net/Bitbucket'
import PullRequestView from '../../../views/bitbucket/pullRequest/PullRequest'
import analytics from '../../../common/analytics'

export default class Show extends ActionPushController {
  execute ({ owner, repo, id }) {
    const commentsFields = [
      'values.id',
      'values.user.display_name',
      'values.created_on',
      'values.content.html',
      'values.parent',
      'values.inline',
      'size',
      'pagelen',
    ].join(',')

    const statusesFields = [
      'values.description',
      'values.key',
      'values.state',
      'values.refname',
      'size',
    ].join(',')

    const [pullRequest, statuses, comments, currentUser] = new Bitbucket(owner, repo)
      .parallel()
      .getPullRequest(id, {
        fields: '+destination.repository.is_private,+destination.repository.owner',
      })
      .getPullRequestStatusList(id, { fields: statusesFields })
      .getPullRequestComments(id, {
        pagelen: 10,
        sort: '-created_on',
        q: 'deleted != true AND content.raw != null AND content.raw != ""',
        fields: commentsFields,
      })
      .getCurrentUser({ fields: 'uuid' })
      .fetch()

    analytics.pageView('Bitbucket Pull Request')

    return new PullRequestView({
      pullRequest,
      statuses,
      comments,
      currentUser,
    })
  }
}
