import ActionPushController from '../../ActionPushController'
import PullRequestDescriptionView from '../../../views/bitbucket/pullRequest/Description'
import analytics from '../../../common/analytics'

export default class extends ActionPushController {
  execute ({ pullRequest }) {
    analytics.event({
      category: 'Bitbucket Pull Request',
      action: 'Show description',
    })

    return new PullRequestDescriptionView({ pullRequest })
  }
}
