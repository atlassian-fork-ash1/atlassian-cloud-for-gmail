import ActionController from '../../../ActionController'
import Bitbucket from '../../../../common/net/Bitbucket'
import AddonError from '../../../../common/AddonError'
import ShowController from '../../pullRequest/Show'
import analytics from '../../../../common/analytics'

export default class extends ActionController {
  execute ({ owner, repo, id, content }) {
    if (!content) {
      throw new AddonError('Empty comments are not allowed', 'VALIDATION_ERROR', 'BITBUCKET')
    }

    new Bitbucket(owner, repo).createPullRequestComment(id, { content })

    analytics.event({
      category: 'Bitbucket Pull Request',
      action: 'Create comment',
    })

    return new ShowController({ event: this.event })
      .execute({ owner, repo, id })
  }
}
