import ActionPushController from '../../../ActionPushController'
import PullRequestCommentView from '../../../../views/bitbucket/pullRequest/Comment'

export default class extends ActionPushController {
  execute ({ pullRequest, comment }) {
    return new PullRequestCommentView({ pullRequest, comment })
  }
}
