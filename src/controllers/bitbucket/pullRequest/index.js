import Preview from './Preview'
import Show from './Show'
import ShowDescription from './ShowDescription'
import comment from './comment'
import merge from './merge'

export default {
  Preview,
  Show,
  ShowDescription,
  comment,
  merge,
}
