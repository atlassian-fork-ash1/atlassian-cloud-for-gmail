import { get } from 'lodash'
import ActionPopController from '../../../ActionPopController'
import Bitbucket from '../../../../common/net/Bitbucket'
import ShowController from '../Show'
import AddonError from '../../../../common/AddonError'
import analytics from '../../../../common/analytics'

export default class extends ActionPopController {
  execute ({
    owner,
    repo,
    id,
    mergeStrategy,
    closeSourceBranch,
  }) {
    try {
      new Bitbucket(owner, repo).mergePullRequest(id, {
        merge_strategy: mergeStrategy,
        close_source_branch: Boolean(closeSourceBranch),
      })

      analytics.event({
        category: 'Bitbucket Pull Request',
        action: 'Merge',
      })

      return new ShowController({ event: this.event }).execute({ owner, repo, id })
    } catch (error) {
      const fields = get(error, 'response.body.error.fields')
      const message = fields
        ? Object.values(fields).map(f => f.join('\n')).join('\n')
        : get(error, 'response.body.error.message')

      if (message) {
        throw new AddonError(message, 'MERGE_ERROR', 'BITBUCKET')
      }

      throw error
    }
  }
}
