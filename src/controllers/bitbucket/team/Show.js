import ActionPushController from '../../ActionPushController'
import Bitbucket from '../../../common/net/Bitbucket'
import TeamView from '../../../views/bitbucket/Team'
import analytics from '../../../common/analytics'

export default class extends ActionPushController {
  execute ({ username }) {
    const team = new Bitbucket().getTeam(username)

    analytics.pageView('Bitbucket Team')

    return new TeamView({ team })
  }
}
