import ActionPushController from '../../ActionPushController'
import Bitbucket from '../../../common/net/Bitbucket'
import UserView from '../../../views/bitbucket/User'
import analytics from '../../../common/analytics'

export default class extends ActionPushController {
  execute ({ username }) {
    const user = new Bitbucket().getUser(username)

    analytics.pageView('Bitbucket User')

    return new UserView({ user })
  }
}
