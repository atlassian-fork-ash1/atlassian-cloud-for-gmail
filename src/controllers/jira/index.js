import issue from './issue'
import user from './user'

export default {
  issue,
  user,
}
