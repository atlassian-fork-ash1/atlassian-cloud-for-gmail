import urlParse from 'url-parse'
import UpdateDraftActionController from '../../UpdateDraftActionController'
import Jira from '../../../common/net/Jira'

export default class extends UpdateDraftActionController {
  execute ({ issue, icon }) {
    // extract cloud id from url
    const cloudId = urlParse(issue.self).pathname.match(/\/ex\/jira\/(.+?)\//)[1] // e.g. /ex/jira/1337

    const serverInfo = new Jira().getServerInfo({ cloudId })
    const url = `${serverInfo.baseUrl}/browse/${issue.key}`

    return this._getHtmlContent({
      url,
      icon,
      issueKey: issue.key,
      summary: issue.fields.summary,
      projectName: issue.fields.project.name,
    })
  }

  _getHtmlContent ({ url, icon, issueKey, summary, projectName }) {
    const link = content => `
      <a href="${url}" style="display:block; color:inherit; text-decoration:none; white-space:nowrap; text-overflow:ellipsis; overflow:hidden;">
        ${content}
      </a>
    `

    return `
      <table style="background-color:#f3f5f7; border-radius:0.5em; padding:0.5em; width:20em; table-layout:fixed;">
        <colgroup>
          <col style="width: 3em;"/>
          <col/>
        </colgroup>
        <tbody>
          <tr>
            <td colspan="2" style="color:#838591">${link(`Jira / ${projectName}`)}</td>
          </tr>
          <tr>
            <td>
              ${link(`<img src="${icon}" style="width: 2.5em;"/>`)}
            </td>
            <td style="color:#42526e;">
              ${link(`
                <b>${issueKey}</b>
                <br/>
                <span title="${summary}">${summary}</span>
              `)}
            </td>
          </tr>
        </tbody>
      </table>
    `
  }
}
