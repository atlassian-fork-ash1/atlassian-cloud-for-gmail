import ActionPushController from '../../ActionPushController'
import Jira from '../../../common/net/Jira'

export default class extends ActionPushController {
  execute ({ instanceHost, issueKey }) {
    const fields = [
      'project',
      'summary',
      'key',
      'status',
      'updated',
      'issuetype',
    ].join(',')

    const issue = new Jira(instanceHost).getIssue(issueKey, { fields })

    return {
      source: issue.fields.project.name,
      title: issue.fields.summary,
      key: issue.key,
      status: issue.fields.status.name,
      updatedAt: issue.fields.updated,
      statusCategory: issue.fields.status.statusCategory.key,
      iconUrl: issue.fields.issuetype.iconUrl,
    }
  }
}
