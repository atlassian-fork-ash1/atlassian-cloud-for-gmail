import ActionPushController from '../../ActionPushController'
import Jira from '../../../common/net/Jira'
import IssueView from '../../../views/jira/issue/Issue'
import analytics from '../../../common/analytics'

export default class extends ActionPushController {
  execute ({ instanceHost, issueKey }) {
    const fields = [
      'project',
      'issuetype',
      'description',
      'summary',
      'status',
      'assignee',
      'labels',
      'priority',
      'timetracking',
      'reporter',
      'components',
      'attachment',
      'watches',
    ].join(',')

    const [issue, issueComments, currentUser, priorities] = new Jira(instanceHost)
      .parallel()
      .getIssue(issueKey, { expand: 'renderedFields,editmeta,transitions', fields })
      .getIssueComments(issueKey, {
        maxResults: 10,
        orderBy: '-created',
        expand: 'renderedBody',
        fields: 'renderedBody,author,created',
      })
      .getCurrentUser()
      .getPriorities()
      .fetch()

    const editableFields = Object.keys(issue.editmeta.fields)

    analytics.pageView('Jira Issue')

    return new IssueView({
      instanceHost,
      issue,
      issueComments,
      currentUser,
      priorities,
      editableFields,
    })
  }
}
