import { get } from 'lodash'
import ActionPopController from '../../../ActionPopController'
import Jira from '../../../../common/net/Jira'
import AddonError from '../../../../common/AddonError'
import analytics from '../../../../common/analytics'
import ShowIssue from '../Show'
import { findUserByEmail } from '../utils'

export default class extends ActionPopController {
  execute ({
    instanceHost,
    issueKey,
    summary,
    transition,
    assigneeEmail,
    description,
    priority,
  }) {
    const jira = new Jira(instanceHost)
    const fields = {}

    if (summary) fields.summary = summary
    if (description) fields.description = description
    if (priority) fields.priority = { id: priority }
    if (assigneeEmail) fields.assignee = findUserByEmail(instanceHost, assigneeEmail)

    try {
      const updateIssue = jira.parallel()
        .updateIssue(issueKey, { fields })

      if (transition) {
        updateIssue.createIssueTransition(issueKey, {
          transition: { id: transition }, // TODO: handle transition required fields
        })
      }

      updateIssue.fetch()
    } catch (error) {
      const errors = get(error, 'response.body.errors')

      if (errors) {
        // jira returns user-friendly error messages
        throw new AddonError(Object.values(errors).join('\n'), 'VALIDATION_ERROR', 'JIRA')
      }

      throw error
    }

    analytics.event({
      category: 'Jira Issue',
      action: 'Edit',
    })

    return new ShowIssue({ event: this.event }).execute({ instanceHost, issueKey })
  }
}
