import ActionPushController from '../../../ActionPushController'
import IssueEditFormView from '../../../../views/jira/issue/EditForm'

export default class extends ActionPushController {
  execute ({ instanceHost, issue, priorities, editableFields }) {
    return new IssueEditFormView({
      instanceHost,
      issue,
      editableFields,
      priorities,
    })
  }
}
