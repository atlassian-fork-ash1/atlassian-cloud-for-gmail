import Preview from './Preview'
import Show from './Show'
import ShowDescription from './ShowDescription'
import ShowAttachments from './ShowAttachments'
import Watch from './Watch'
import InsertLink from './InsertLink'
import comment from './comment'
import editForm from './editForm'
import assignee from './assignee'

export default {
  Preview,
  Show,
  ShowDescription,
  ShowAttachments,
  Watch,
  InsertLink,
  comment,
  editForm,
  assignee,
}
