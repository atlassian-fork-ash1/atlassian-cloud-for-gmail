import AddonError from '../../../common/AddonError'
import Jira from '../../../common/net/Jira'

// eslint-disable-next-line import/prefer-default-export
export function findUserByEmail (instanceHost, email) {
  if (email === 'Unassigned') return { name: null }

  // there is no API in Jira to get user by email
  const users = new Jira(instanceHost).searchUsers({ username: email })

  if (users.length !== 1) { // should be exactly 1 user
    throw new AddonError(`User with email <b>${email}</b> not found`, 'VALIDATION_ERROR', 'JIRA')
  }

  return { name: users[0].name }
}
