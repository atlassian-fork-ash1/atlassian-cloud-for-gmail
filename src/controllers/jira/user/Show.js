import ActionPushController from '../../ActionPushController'
import Jira from '../../../common/net/Jira'
import UserView from '../../../views/jira/User'
import analytics from '../../../common/analytics'

export default class extends ActionPushController {
  execute ({ instanceHost, userName }) {
    const fields = [
      'avatarUrls',
      'displayName',
      'emailAddress',
      'name',
      'timeZone',
    ].join(',')

    const user = new Jira(instanceHost).getUser({ username: userName, fields })

    analytics.pageView('Jira User')

    return new UserView({ instanceHost, user })
  }
}
