import { get } from 'lodash'

const Colors = {
  red: '#de350b',
  yellow: '#ff8b00',
  green: '#00875a',
  blue: '#0052cc',
  default: '#42526e',
}

const statusColorsMap = {
  bitbucket: {
    pipeline: {
      SUCCESSFUL: Colors.green,
      FAILED: Colors.red,
      INPROGRESS: Colors.blue,
      STOPPED: Colors.yellow,
    },
    pullRequest: {
      MERGED: Colors.green,
      SUPERSEDED: Colors.yellow,
      OPEN: Colors.blue,
      DECLINED: Colors.red,
    },
    issue: {
      'on hold': Colors.yellow,
      new: Colors.default,
      open: Colors.default,
      resolved: Colors.green,
      duplicate: Colors.yellow,
      invalid: Colors.red,
      wontfix: Colors.red,
      closed: Colors.green,
    },
  },
  jira: {
    issue: {
      new: Colors.default,
      indeterminate: Colors.blue,
      done: Colors.green,
    },
  },
}

export const pickStatusColor = (status, product, entity) => get(
  statusColorsMap,
  `${product}.${entity}.${status}`,
  Colors.default
)

export default Colors
