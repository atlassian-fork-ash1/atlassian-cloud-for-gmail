import mimeFormat from 'mime-format'

const CDN_PATH = 'https://partner-engineering-cdn.atl-paas.net'

export const buildIconUrl = fileName => `${CDN_PATH}/icons/${fileName}.png`
export const buildImageUrl = fileName => `${CDN_PATH}/${fileName}.png`

const Icons = {
  attachment: buildIconUrl('attachment'),
  email: buildIconUrl('email-filled'),
  location: buildIconUrl('location'),
  settings: buildIconUrl('settings'),
  unassigned: buildIconUrl('unknown-user'),
  time: buildIconUrl('time'),
  link: buildIconUrl('link'),
  lock: buildIconUrl('lock'),
  edit: buildIconUrl('edit'),
  warning: buildIconUrl('warning'),
  success: buildIconUrl('success'),
  error: buildIconUrl('error'),
  open: buildIconUrl('open-attachment'),
  merge: buildIconUrl('merge'),
  feedback: buildIconUrl('feedback'),
}

export const ProductIcons = {
  atlassian: buildImageUrl('atlassian-logo-blue'),
  jira: buildImageUrl('jira-logo-blue'),
  bitbucket: buildImageUrl('bitbucket-logo-blue'),
  jiraGray: buildImageUrl('jira-logo-gray'),
}

export const BitbucketEntityIcons = {
  issue: buildIconUrl('bitbucket-issue'),
  pipeline: buildIconUrl('bitbucket-pipeline'),
  pullRequest: buildIconUrl('bitbucket-pull-request'),
}

export const BuildIcons = {
  PENDING: buildIconUrl('build-inprogress'),
  IN_PROGRESS: buildIconUrl('build-inprogress'),
  ERROR: Icons.error,
  EXPIRED: Icons.error,
  FAILED: buildIconUrl('build-failed'),
  SKIPPED: Icons.error,
  STOPPED: buildIconUrl('build-stopped'),
  SUCCESSFUL: buildIconUrl('build-passed'),
}

export const MediaIcons = {
  image: buildIconUrl('media-image'),
  video: buildIconUrl('media-video'),
  audio: buildIconUrl('media-audio'),
  pdf: buildIconUrl('media-pdf'),
  document: buildIconUrl('media-document'),
  genericDocument: buildIconUrl('media-generic-document'),
}

export function getMimeTypeIcon (mimeType) {
  const iconsMap = {
    text: MediaIcons.document,
    image: MediaIcons.image,
    audio: MediaIcons.audio,
    video: MediaIcons.video,
    embed: MediaIcons.pdf,
  }

  const { type } = mimeFormat.guess(mimeType)

  return iconsMap[type] || MediaIcons.genericDocument
}

export default Icons
