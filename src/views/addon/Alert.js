import Icons from './../Icons'
import View from '../View'

export default class extends View {
  get header () {
    const { title = 'Error', type = 'error' } = this.props

    return CardService.newCardHeader()
      .setTitle(title)
      .setImageUrl(Icons[type])
  }

  get sections () {
    const { text } = this.props

    if (!text) return null

    return CardService.newCardSection()
      .addWidget(CardService.newTextParagraph().setText(text))
  }
}
