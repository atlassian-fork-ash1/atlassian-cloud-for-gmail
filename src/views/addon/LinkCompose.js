import View from '../View'
import Colors from '../Colors'
import {
  colorString,
  createAction,
  createActionButtonWidget,
  createKeyValueWidget,
  formatJiraIconUrl
} from '../utils'

export default class extends View {
  get header () {
    return CardService.newCardHeader()
      .setTitle('Insert Link')
  }

  get sections () {
    return this._getJiraIssuesSection()
  }

  _getJiraIssuesSection () {
    const textInput = CardService.newTextInput()
      .setTitle('Search Jira issues')
      .setFieldName('issueQuery')

    const section = CardService.newCardSection()
      .addWidget(textInput)
      .addWidget(createActionButtonWidget('Search', 'addon.ShowCompose'))

    const { issues } = this.props

    if (!issues.length) {
      return section.addWidget(createKeyValueWidget({
        content: 'No issues were found.',
      }))
    }

    issues.forEach(i => section.addWidget(this._getIssueWidget(i)))

    return section
  }

  _getIssueWidget (issue) {
    const { key, fields } = issue
    const { summary, issuetype } = fields

    // We don't know instanceHost at this point so at least
    // let's try to get an icon in png despite the url query mess cases
    const icon = formatJiraIconUrl(issuetype.iconUrl, null, true)

    return createKeyValueWidget({
      icon,
      content: `${colorString(key, Colors.default)} • ${summary}`,
      action: createAction('jira.issue.InsertLink', { issue, icon }),
    })
  }
}
