import urlParse from 'url-parse'
import View from '../View'
import Icons, { ProductIcons } from '../Icons'
import {
  createActionButtonWidget,
  createKeyValueWidget,
  createLinkButtonWidget
} from '../utils'

export default class extends View {
  get header () {
    return CardService.newCardHeader()
      .setTitle('Settings')
      .setImageUrl(Icons.settings)
  }

  get sections () {
    return [
      this._getJiraSection(),
      this._getBitbucketSection(),
    ]
  }

  _getJiraSection () {
    const { jiraUser, jiraAuthUrl } = this.props

    const cardSection = CardService.newCardSection()
      .setHeader(`<b>Jira Cloud</b>`)

    if (jiraUser) {
      cardSection
        .addWidget(createKeyValueWidget({
          content: jiraUser.name,
          icon: jiraUser.picture,
          bottomLabel: jiraUser.email,
        }))
    } else {
      cardSection
        .addWidget(CardService.newTextParagraph().setText('Connect your Jira site to Gmail'))
    }

    const authAction = CardService.newAuthorizationAction()
      .setAuthorizationUrl(jiraAuthUrl)

    const authText = 'Connect Jira Site'
    const authButton = CardService.newTextButton()
      .setText(authText)
      .setAuthorizationAction(authAction)

    if (jiraUser) {
      jiraUser.sites.forEach((site) => {
        cardSection.addWidget(createKeyValueWidget({
          content: urlParse(site.baseUrl).hostname,
          multiline: true,
          icon: ProductIcons.jiraGray,
        }))
      })

      // url to any Jira instance Gmail app has access to
      const removeAccessUrl = `${jiraUser.sites[0].baseUrl}/people/${jiraUser.account_id}/settings/apps`

      cardSection
        .addWidget(authButton)
        .addWidget(createLinkButtonWidget('Remove Access', removeAccessUrl))

      return cardSection
    }

    return cardSection.addWidget(authButton)
  }

  _getBitbucketSection () {
    const { bitbucketUser, bitbucketAuthUrl } = this.props

    const cardSection = CardService.newCardSection()
      .setHeader(`<b>Bitbucket Cloud</b>`)

    if (!bitbucketUser) {
      const action = CardService.newAuthorizationAction()
        .setAuthorizationUrl(bitbucketAuthUrl)

      cardSection
        .addWidget(CardService.newTextParagraph()
          .setText('Connect your Bitbucket account to Gmail'))
        .addWidget(CardService.newTextButton()
          .setText('Connect Bitbucket')
          .setAuthorizationAction(action))

      return cardSection
    }

    cardSection
      .addWidget(createKeyValueWidget({
        content: bitbucketUser.display_name,
        icon: bitbucketUser.links.avatar.href,
        bottomLabel: bitbucketUser.email,
      }))
      .addWidget(createActionButtonWidget('Disconnect Bitbucket', 'addon.Disconnect', { product: 'Bitbucket' }))

    return cardSection
  }
}
