import View from '../../View'
import { ProductIcons, buildImageUrl } from '../../Icons'
import { createKeyValueWidget, createAction } from '../../utils'

export default class extends View {
  get header () {
    return CardService.newCardHeader()
      .setTitle('Connect Jira Cloud')
      .setSubtitle('Step 1 of 2')
      .setImageUrl(ProductIcons.jira)
  }

  get sections () {
    const image = CardService.newImage()
      .setImageUrl(buildImageUrl('onboarding-jira'))

    const paragraph = CardService.newTextParagraph()
      .setText('Keep everyone in sync, everywhere with updates on tasks, stories and requests from your inbox.')

    const authButton = CardService.newTextButton()
      .setText('Connect Jira Cloud Site')
      .setAuthorizationAction(CardService.newAuthorizationAction()
        .setAuthorizationUrl(this.props.authUrl))

    const rejectionWidget = createKeyValueWidget({
      content: `No thanks, I don't use Jira Cloud`,
      action: createAction('addon.Onboarding', { onboardingState: 'bitbucket' }),
    })

    return CardService.newCardSection()
      .addWidget(image)
      .addWidget(paragraph)
      .addWidget(authButton)
      .addWidget(rejectionWidget)
  }
}
