import mime from 'mime-types'
import BitbucketIssueView from './Issue'
import { getMimeTypeIcon } from '../../Icons'
import { createKeyValueWidget } from '../../utils'

export default class extends BitbucketIssueView {
  get url () {
    return `${super.url}/attachments`
  }

  get sections () {
    const cardSection = CardService.newCardSection()
      .setHeader('<b>Attachments</b>')

    this.props.attachments.values.forEach((a) => {
      cardSection.addWidget(this._getAttachmentWidget(a))
    })

    return cardSection
  }

  _getAttachmentWidget (attachment) {
    const mimeType = mime.lookup(attachment.name) // get mimetype from file's extension

    return createKeyValueWidget({
      content: attachment.name,
      icon: getMimeTypeIcon(mimeType),
    })
  }
}
