import BitbucketView from '../Bitbucket'
import widgets from '../widgets'
import Icons from '../../Icons'

import {
  createAction,
  createKeyValueWidget,
  createActionButtonWidget,
  formatDateTime,
  capitalizeString,
  colorString,
  pluralizeString,
  createExternalLink,
  formatHtml
} from '../../utils'
import Colors, { pickStatusColor } from '../../Colors'

export default class extends BitbucketView {
  get url () {
    return this._getEntityLink(this.props.issue)
  }

  get header () {
    const { issue } = this.props

    return CardService.newCardHeader()
      .setTitle(issue.title)
      .setSubtitle(`Issue #${issue.id} · ${formatDateTime(issue.updated_on)}`)
  }

  get sections () {
    return [
      this._getDetailSection(),
      this._getActionsSection(),
      this._getCommentsSection(),
    ]
  }

  _getDetailSection () {
    const { issue, attachmentsSize } = this.props
    const {
      repository,
      kind,
      state,
      assignee,
      priority,
      reporter,
      votes,
      content,
    } = issue

    const cardSection = CardService.newCardSection()
      .addWidget(createKeyValueWidget({
        label: 'Repository',
        content: repository.name,
        icon: repository.links.avatar.href,
      }))
      .addWidget(createKeyValueWidget({
        label: 'Issue Type',
        content: capitalizeString(kind),
      }))
      .addWidget(widgets.basic('Status', colorString(capitalizeString(state), pickStatusColor(
        state,
        'bitbucket',
        'issue'
      ))))
      .addWidget(widgets.account(assignee, 'Assignee', 'Unassigned'))
      .addWidget(widgets.basic('Priority', capitalizeString(priority)))
      .addWidget(createKeyValueWidget({
        label: 'Description',
        content: formatHtml({
          html: content.html,
          defaultValue: 'No description.',
        }),
        multiline: true,
        action: createAction('bitbucket.issue.ShowDescription', { issue }),
      }))

    if (attachmentsSize !== undefined) { // expect positive int or 0
      const widgetParams = {
        icon: Icons.attachment,
        content: pluralizeString(attachmentsSize, 'attachment'),
      }

      if (attachmentsSize > 0) { // make it clickable if there are attachments
        widgetParams.action = createAction(
          'bitbucket.issue.ShowAttachments',
          { issue }
        )
      }

      cardSection.addWidget(createKeyValueWidget(widgetParams))
    }

    cardSection
      .addWidget(widgets.account(reporter, 'Reporter'))
      .addWidget(widgets.basic('Created', formatDateTime(issue.created_on)))
      .addWidget(widgets.basic('Votes', votes.toString()))

    return cardSection
  }

  _getCommentsSection () {
    const { issue, comments } = this.props
    const { size, pagelen, values: commentsValues } = comments

    const cardSection = CardService.newCardSection()

    if (size > 3) { // leaves exactly 3 top comments
      cardSection
        .setCollapsible(true)
        .setNumUncollapsibleWidgets(5) // cuz input + button + 3 comments
    }

    cardSection
      .addWidget(widgets.commentInput())
      .addWidget(createActionButtonWidget('Save Comment', 'bitbucket.issue.comment.Create', this._getIssueActionParams()))

    commentsValues.forEach((comment) => {
      const action = createAction('bitbucket.issue.comment.Show', { comment, issue })
      cardSection.addWidget(widgets.comment({ comment, action }))
    })

    if (size > pagelen) {
      cardSection
        .addWidget(createKeyValueWidget({
          content: colorString(`Show <b>${size}</b> comments in Bitbucket`, Colors.blue),
          openLink: createExternalLink(`${this.url}/comments`),
        }))
    }

    return cardSection
  }

  _getActionsSection () {
    const buttonsSet = CardService.newButtonSet()
      .addButton(createActionButtonWidget('Edit Issue', 'bitbucket.issue.editForm.Show', { issue: this.props.issue }))

    if (this.props.isWatching !== undefined) { // expect a boolean value
      const { isWatching } = this.props
      const buttonText = isWatching ? 'Stop watching' : 'Watch'

      buttonsSet.addButton(createActionButtonWidget(buttonText, 'bitbucket.issue.Watch', {
        ...this._getIssueActionParams(),
        watch: !isWatching,
      }))
    }

    return CardService.newCardSection().addWidget(buttonsSet)
  }

  _getIssueActionParams () {
    const { issue } = this.props

    return {
      owner: issue.repository.owner.username,
      repo: issue.repository.name,
      id: issue.id,
    }
  }
}
