import PullRequestView from './PullRequest'
import widgets from '../widgets'
import { createActionButtonWidget } from '../../utils'

export default class extends PullRequestView {
  get url () {
    return this._getEntityLink(this.props.comment)
  }

  get sections () {
    const { pullRequest, comment } = this.props

    return CardService.newCardSection()
      .addWidget(widgets.comment({ comment, truncate: false }))
      .addWidget(widgets.commentInput())
      .addWidget(createActionButtonWidget('Save Comment', 'bitbucket.pullRequest.comment.Reply', {
        pullRequest,
        comment,
      }))
  }

  setProductLinkButtonSection () {
    return super.setProductLinkButtonSection(`View Comment In ${this.product}`)
  }
}
