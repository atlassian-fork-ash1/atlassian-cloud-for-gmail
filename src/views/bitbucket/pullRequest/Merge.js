import BitbucketView from '../Bitbucket'
import { createActionButtonWidget } from '../../utils'
import Icons from '../../Icons'
import widgets from '../widgets'

export default class extends BitbucketView {
  get url () {
    return this._getEntityLink(this.props.pullRequest)
  }

  get header () {
    return CardService.newCardHeader()
      .setTitle('Merge pull request')
      .setImageUrl(Icons.merge)
  }

  get sections () {
    return [
      this._branchSection(),
      this._mergeStrategySection(),
      this._closeSourceBranchSection(),
      this._buttonsSection(),
    ]
  }

  _branchSection () {
    const { source, destination } = this.props.pullRequest

    return CardService.newCardSection()
      .setHeader('<b>Branch</b>')
      .addWidget(widgets.basic('Source', source.branch.name))
      .addWidget(widgets.basic('Destination', destination.branch.name))
  }

  _mergeStrategySection () {
    return CardService.newCardSection()
      .setHeader('<b>Merge strategy</b>')
      .addWidget(CardService.newSelectionInput()
        .setType(CardService.SelectionInputType.RADIO_BUTTON)
        .setFieldName('mergeStrategy')
        .addItem('Merge commit (git merge --no-ff)', 'merge_commit', true)
        .addItem('Squash (git merge --squash)', 'squash', false)
        .addItem('Fast forward (git merge --ff-only)', 'fast_forward', false))
  }

  _closeSourceBranchSection () {
    return CardService.newCardSection()
      .addWidget(CardService.newSelectionInput()
        .setType(CardService.SelectionInputType.CHECK_BOX)
        .setFieldName('closeSourceBranch')
        .addItem('Close source branch after merge', 'close', false))
  }

  _buttonsSection () {
    const { pullRequest } = this.props

    const prData = {
      owner: pullRequest.destination.repository.owner.username,
      repo: pullRequest.destination.repository.name,
      id: pullRequest.id,
    }

    return CardService.newCardSection()
      .addWidget(CardService.newButtonSet()
        .addButton(createActionButtonWidget('Merge', 'bitbucket.pullRequest.merge.Merge', prData))
        .addButton(createActionButtonWidget('Cancel', 'addon.NavigateBack')))
  }
}
