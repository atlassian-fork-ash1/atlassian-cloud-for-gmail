import { get } from 'lodash'
import Icons from '../../Icons'
import { createKeyValueWidget, createAction } from '../../utils'

export default (account, label = 'Account', content = '-') => {
  const widgetData = {
    label,
    icon: get(account, 'links.avatar.href', Icons.unassigned),
    content,
  }

  if (!account) return createKeyValueWidget(widgetData)

  const widget = createKeyValueWidget({
    ...widgetData,
    content: account.display_name,
  })

  if (account.type === 'user') {
    widget.setOnClickAction(createAction('bitbucket.user.Show', { username: account.username }))
  } else if (account.type === 'team') {
    widget.setOnClickAction(createAction('bitbucket.team.Show', { username: account.username }))
  }

  return widget
}
