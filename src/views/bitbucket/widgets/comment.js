import truncatePath from 'gentle-path-truncate'
import {
  createKeyValueWidget,
  formatDateTime,
  formatHtml
} from '../../utils'

export default ({ comment, action, multiline = true, truncate = true }) => {
  const label = `${comment.user.display_name} ${formatDateTime(comment.created_on)}`

  const data = {
    label: comment.parent ? `⤺ ${label}` : label,
    content: formatHtml({
      html: comment.content.html,
      defaultValue: '-',
      truncate,
    }),
    multiline,
  }

  if (action) {
    data.action = action
  }

  if (comment.inline) {
    const { path, to } = comment.inline
    let maxlen = 40
    let toStr = ''

    if (to) {
      toStr = ` on line: ${to}`
      maxlen -= toStr.length
    }

    data.bottomLabel = `${truncatePath(path, maxlen)}${toStr}`
  }

  return createKeyValueWidget(data)
}
