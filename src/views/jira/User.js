import Icons from '../Icons'
import JiraView from './Jira'
import {
  createKeyValueWidget,
  formatDateWithTimezone
} from '../utils'

export default class extends JiraView {
  get url () {
    const { instanceHost, user: { name } } = this.props

    return `https://${instanceHost}/secure/ViewProfile.jspa?name=${name}`
  }

  get header () {
    const { user } = this.props

    return CardService.newCardHeader()
      .setTitle(user.displayName)
      .setImageUrl(user.avatarUrls['32x32'])
  }

  get sections () {
    const { user } = this.props
    const cardSection = CardService.newCardSection()
    cardSection.addWidget(createKeyValueWidget({
      content: user.emailAddress,
      icon: Icons.email,
    }))

    if (user.timeZone) {
      cardSection
        .addWidget(createKeyValueWidget({
          content: user.timeZone,
          icon: Icons.location,
        }))
        .addWidget(createKeyValueWidget({
          content: formatDateWithTimezone({
            date: Date.now(),
            timeZone: user.timeZone,
            format: 'dddd, h:mm A',
          }),
          icon: Icons.time,
        }))
    }

    return cardSection
  }
}
