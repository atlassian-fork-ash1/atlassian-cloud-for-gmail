import JiraView from '../Jira'
import widgets from '../widgets'
import { createAction, createActionButtonWidget } from '../../utils'

export default class extends JiraView {
  get sections () {
    return CardService.newCardSection()
      .setHeader('<b>Assign</b>')
      .addWidget(widgets.user({
        user: this.props.issue.fields.assignee,
        label: 'Assignee',
        instanceHost: this.props.instanceHost,
      }))
      .addWidget(this._getAutocompleteInput())
      .addWidget(this._getButtons())
  }

  _getAutocompleteInput () {
    return CardService.newTextInput()
      .setFieldName('assigneeEmail')
      .setTitle('New assignee')
      .setHint('Enter email address or name')
      .setSuggestionsAction(createAction('jira.user.Search', {
        instanceHost: this.props.instanceHost,
        projectId: this.props.issue.fields.project.id,
      }))
  }

  _getButtons () {
    const { instanceHost, issue } = this.props

    return CardService.newButtonSet()
      .addButton(createActionButtonWidget('Save Changes', 'jira.issue.assignee.Save', {
        instanceHost, issueKey: issue.key,
      }))
      .addButton(createActionButtonWidget('Cancel', 'addon.NavigateBack'))
  }
}
