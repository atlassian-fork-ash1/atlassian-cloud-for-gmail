import { get } from 'lodash'
import { createKeyValueWidget, formatDateTime, formatHtml } from '../../utils'

export default ({ comment, action, multiline = true, truncate = true }) => {
  const data = {
    label: `${get(comment, 'author.displayName', 'Anonymous')} ${formatDateTime(comment.created)}`,
    content: formatHtml({
      html: comment.renderedBody,
      defaultValue: '-',
      truncate,
    }),
    multiline,
  }

  if (action) {
    data.action = action
  }

  return createKeyValueWidget(data)
}
