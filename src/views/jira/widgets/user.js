import Icons from '../../Icons'
import { createKeyValueWidget, createAction } from '../../utils'

export default ({ instanceHost, user, label = 'User' }) => {
  if (!user) {
    return createKeyValueWidget({
      label,
      icon: Icons.unassigned,
      content: 'Unassigned',
    })
  }

  return createKeyValueWidget({
    label,
    icon: user.avatarUrls['48x48'],
    content: user.displayName,
    action: createAction('jira.user.Show', { instanceHost, userName: user.key }),
  })
}
